webpackJsonp(["Subscribers.module"],{

/***/ "../../../../../src/app/theme/Subscribers/Subscribers.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"error-div\" class=\"row\" style=\"display: none\">\r\n    <div class=\"col-md-12 block\">\r\n        <p class=\"generic-error\">{{DAPPError}}</p>\r\n    </div>\r\n</div>\r\n<div id=\"working-div\">\r\n    <div id=\"userList\">\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-12\">\r\n\r\n                <app-card [title]=\"'Subscribers'\" [cardOptionBlock]=\"true\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-sm-12\">\r\n                            <label class=\"dt-cust-search f-right\">\r\n                <div class=\"form-group\">\r\n                  <label>Search: </label>\r\n                            <input type='text' class=\"form-control input-sm m-l-10\" placeholder='Search Email' (keyup)='updateFilter($event)' />\r\n                        </div>\r\n                        </label>\r\n                    </div>\r\n            </div>\r\n            <ngx-datatable #userListTable class='data-table' [loadingIndicator]=\"loadingIndicator\" [scrollbarH]=\"true\" [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"50\" [limit]=\"10\" [rows]='rowsFilter' (select)='onSelect($event)'>\r\n                <ngx-datatable-column name=\"Email\" sortable=\"false\" prop=\"Email\" style=\"width:40%\">\r\n                    <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                        {{value}}\r\n                    </ng-template>\r\n                </ngx-datatable-column>\r\n                <ngx-datatable-column name=\"Subscription Date\" sortable=\"false\" prop=\"CreatedOnUTC\" style=\"width:40%\">\r\n                    <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                        {{value | date:mediumDate}}\r\n                    </ng-template>\r\n                </ngx-datatable-column>\r\n            </ngx-datatable>\r\n            </app-card>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div id=\"detail\">\r\n    <div class=\"tableDiv\">\r\n        <button class=\"btn btn-primary go-back\" (click)=\"back()\">Go Back</button>\r\n        <button class=\"btn btn-primary go-back\" style=\"float: right;margin-left:10px\" (click)=\"referrals()\">Referrals</button>\r\n        <button class=\"btn btn-primary go-back\" style=\"float: right;\" (click)=\"kycdetail()\">KYC Detail</button>\r\n        <table class=\"table table-hover\" style=\"background:white;\">\r\n            <tbody>\r\n                <tr>\r\n                    <th>Email</th>\r\n                    <td>{{userDetail.Email}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>KYC Status</th>\r\n                    <td>{{userKYCStatus}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>BNZ Token</th>\r\n                    <td>{{userDetail.Tokens}}</td>\r\n                </tr>\r\n                <!-- <tr>\r\n          <th>EARN Coins</th>\r\n          <td>{{userDetail.EARNTokens}}</td>\r\n        </tr> -->\r\n                <tr>\r\n                    <th>\r\n                        Ethereum Address\r\n                    </th>\r\n                    <td>{{userDetail.EthAddress}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>BCH Fund Receiver Address</th>\r\n                    <td>{{userDetail.BCHWalletRecieverAddress}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>ETH Fund Receiver Address</th>\r\n                    <td>{{userDetail.ETHWalletFundRecieverAddress}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>BTC Fund Receiver Address</th>\r\n                    <td>{{userDetail.BTCWalletRecieverAddress}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <th>LTC Fund Receiver Address</th>\r\n                    <td>{{userDetail.LTCWalletRecieverAddress}}</td>\r\n                </tr>\r\n                <!-- <tr>\r\n          <th>BTG Fund Reciever Address</th>\r\n          <td>{{userDetail.BTGWalletRecieverAddress}}</td>\r\n        </tr>\r\n        <tr>\r\n          <th>DASH Fund Reciever Address</th>\r\n          <td>{{userDetail.DASHWalletRecieverAddress}}</td>\r\n        </tr>\r\n        <tr>\r\n          <th>NEO Fund Reciever Address</th>\r\n          <td>{{userDetail.NEOWalletRecieverAddress}}</td>\r\n        </tr> -->\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n    <table class=\"table table-hover\" align=\"center\" style=\"background:white;\">\r\n        <thead>\r\n            <th>Tx Hash</th>\r\n            <th>Amount Receive</th>\r\n            <th>BNZ Coins</th>\r\n            <th>Crypto Live Rate</th>\r\n            <th>Transaction Type</th>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let item of userDetail.allIncomingTransactions\">\r\n                <td style=\"width: 135px;display: block;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;margin: 0;\">\r\n                    <a href=\"{{item.txDetailURL}}\" target=\"_blank\" style=\"color:#c8042b\">{{item.txHash}}</a>\r\n                </td>\r\n                <td *ngIf=\"item.amountRecieve == 0\">N/A</td>\r\n                <td *ngIf=\"item.amountRecieve != 0\">{{item.amountRecieve}}</td>\r\n                <td *ngIf=\"item.totalCoins == 0\">N/A</td>\r\n                <td *ngIf=\"item.totalCoins != 0\">{{item.totalCoins}}</td>\r\n                <td *ngIf=\"item.cryptoLiveRate == 0\">N/A</td>\r\n                <td *ngIf=\"item.cryptoLiveRate != 0\">{{item.cryptoLiveRate}}</td>\r\n                <td>{{item.transactionType}}</td>\r\n            </tr>\r\n            <tr *ngIf=\"show\">\r\n                <td colspan=\"6\">No Transactions made by User\r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n<div id=\"kycdetail\">\r\n    <div class=\"tableDiv\">\r\n        <button class=\"btn btn-primary go-back\" (click)=\"goBackFromKYC()\">Go Back</button>\r\n        <button class=\"btn btn-primary go-back\" style=\"float: right;margin-left: 2%;\" (click)=\"updateKYCStatus(2)\">Reject</button>\r\n        <button class=\"btn btn-primary go-back\" style=\"float: right;\" (click)=\"updateKYCStatus(1)\">Approve</button>\r\n        <div class=\"col-md-12\">\r\n            <p style=\"text-align: center;font-size: 20px;\">KYC Status : {{userKYCStatus}}</p>\r\n        </div>\r\n        <div id=\"kycDetailedDiv\" style=\"display: none\">\r\n            <div class=\"col-md-12\">\r\n                <p style=\"text-align: center;font-size: 20px;\">KYC Detail</p>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-md-5\">\r\n                    <img class=\"img\" style=\"width: 100%;\" src=\"{{passportImageURL}}\" />\r\n                </div>\r\n                <div class=\"col-md-2\"></div>\r\n                <div class=\"col-md-5\">\r\n                    <img class=\"img\" style=\"width: 100%;\" src=\"{{passportSelfieImageURL}}\" />\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div id=\"referraldetail\">\r\n    <div class=\"tableDiv\">\r\n        <button class=\"btn btn-primary go-back\" (click)=\"goBackFromReferral()\">Go Back</button>\r\n        <table class=\"table table-hover\" align=\"center\" style=\"background:white;\">\r\n            <thead>\r\n                <th>Name</th>\r\n                <th>Email</th>\r\n                <th>Tokens</th>\r\n            </thead>\r\n            <tbody>\r\n                <tr *ngFor=\"let item of referredUsers\">\r\n                    <td>{{item.Name}}</td>\r\n                    <td>{{item.Email}}</td>\r\n                    <td>{{item.Tokens}}</td>\r\n                </tr>\r\n                <tr *ngIf=\"show2\">\r\n                    <td colspan=\"6\">No Referred Users\r\n                    </td>\r\n                </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/theme/Subscribers/Subscribers.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#detail {\n  display: none;\n  width: 100%; }\n\n#kycdetail {\n  display: none;\n  width: 100%; }\n\n#referraldetail {\n  display: none;\n  width: 100%; }\n\n.go-back {\n  margin-bottom: 15px; }\n\n.datatable-body-cell-label {\n  margin-top: -10px !important; }\n\n.view-detail-btn {\n  font-size: 15px;\n  font-weight: 500;\n  width: 50%;\n  height: 44px;\n  margin-top: -10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/Subscribers/Subscribers.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubscribersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__web3_contracts_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/web3/contracts/smart.contract.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SubscribersComponent = (function () {
    function SubscribersComponent(_userService, router, _web3ContractService) {
        this._userService = _userService;
        this.router = router;
        this._web3ContractService = _web3ContractService;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.userDetail = [];
        this.userKYCStatus = "";
        this.passportImageURL = "";
        this.passportSelfieImageURL = "";
        this._transactionBaseURL = "";
        this.show = false;
        this.show2 = false;
        this.referredUsers = [];
    }
    SubscribersComponent.prototype.ngOnInit = function () {
        this.getUserList();
    };
    SubscribersComponent.prototype.getUserList = function () {
        var _this = this;
        $('#userList').show();
        this._userService.getSubscribers().subscribe(function (res) {
            _this.rowsFilter = res.message;
            _this.tempFilter = res.message;
            console.log(res);
        });
    };
    SubscribersComponent.prototype.goBackFromKYC = function () {
        $('#detail').show();
        $('#kycdetail').hide();
    };
    SubscribersComponent.prototype.kycdetail = function () {
        $('#kycdetail').show();
        $('#detail').hide();
    };
    SubscribersComponent.prototype.updateKYCStatus = function (val) {
        // this._userService.changeKYCStatus("", val, this.userDetail.Email).subscribe(response => {
        //   var status = 0;
        //   if (val == 1) {
        //     this.userKYCStatus = "Approved";
        //     status = 2;
        //   }
        //   else {
        //     this.userKYCStatus = "Rejected";
        //     status = 3;
        //   }
        //   for (var i = 0; i < this.rowsFilter.length; i++) {
        //     if (this.rowsFilter[i].Email == this.userDetail.Email) {
        //       this.rowsFilter[i].iskycVerified = status;
        //       break;
        //     }
        //   }
        // });
        // $('#userList').hide();
        // $('#kycdetail').hide();
        // $('#detail').show();
    };
    SubscribersComponent.prototype.view = function (id) {
        // this.show = false;
        // this.userDetail = this.rowsFilter.find(function (element) {
        //   return element._id == id;
        // });
        // if (this.userDetail.allIncomingTransactions.length == 0) {
        //   this.show = true;
        // }
        // if (this.userDetail.iskycVerified == 0) {
        //   this.userKYCStatus = "Pending";
        // }
        // else if (this.userDetail.iskycVerified == 1) {
        //   $('#kycDetailedDiv').show();
        //   this.userKYCStatus = "Submitted";
        // }
        // else if (this.userDetail.iskycVerified == 2) {
        //   $('#kycDetailedDiv').show();
        //   this.userKYCStatus = "Approved";
        // }
        // else if (this.userDetail.iskycVerified == 3) {
        //   $('#kycDetailedDiv').show();
        //   this.userKYCStatus = "Rejected";
        // }
        // this.passportImageURL =  this.userDetail.passportNumberImage;
        // this.passportSelfieImageURL = this.userDetail.passportSelfieImage;
        // $('#userList').hide();
        // $('#detail').show();
    };
    SubscribersComponent.prototype.back = function () {
        $('#userList').show();
        $('#detail').hide();
    };
    SubscribersComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        // filter our data
        var temp = this.tempFilter.filter(function (d) {
            return d.Email.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rowsFilter = temp;
        // Whenever the filter changes, always go back to the first page
        this.userListTable.offset = 0;
    };
    SubscribersComponent.prototype.viewUsersRefferalList = function () {
        // this.show2 = false;
        // this.referredUsers = [];
        // this._userService.getUsersReferrals(this.userDetail.Email).subscribe(response => {
        //   this.referredUsers = response.data;
        //   if (this.referredUsers.length == 0) {
        //     this.show2 = true;
        //   }
        // });
    };
    SubscribersComponent.prototype.goBackFromReferral = function () {
        $('#detail').show();
        $('#referraldetail').hide();
    };
    SubscribersComponent.prototype.referrals = function () {
        $('#detail').hide();
        $('#referraldetail').show();
        // this.viewUsersRefferalList();
    };
    SubscribersComponent.prototype.sendTokens = function (id) {
        // debugger;
        // this.rowsFilter = this.tempFilter;
        // var userDetailTokensToSent = this.rowsFilter.find(function (element) {
        //   return element._id == id;
        // });
        // if (userDetailTokensToSent.isTokenSent == false) {
        //   var Total_Tokens = userDetailTokensToSent.Tokens;
        //   var userEthAddress = userDetailTokensToSent.EthAddress;
        //   debugger;
        //   if (userDetailTokensToSent.iskycVerified != 2) {
        //     alert("User KYC is not Verified/Submitted");
        //     return;
        //   }
        //   if (Total_Tokens <= 0) {
        //     alert("User has not purchased any tokens yet");
        //     return;
        //   }
        //   if (userEthAddress === undefined) {
        //     alert("Eth Address is not provided by the User");
        //     return;
        //   }
        //   this._web3ContractService.getEthereumBalance()
        //     .then(eth_balance => {
        //       if (eth_balance <= 0) {
        //         alert("You dont have enough ethers to transfer Tokens");
        //         return;
        //       }
        //       this._web3ContractService.getTokenBalance().then(balance => {
        //         if (balance <= 0) {
        //           alert("You dont have enough balance to transfer Tokens");
        //           return;
        //         }
        //         if (balance < Total_Tokens) {
        //           alert(`Insufficient Token Balance. You have ${balance} tokens`);
        //           return;
        //         }
        //         var tokens = parseFloat(Total_Tokens) * Math.pow(10, this._web3ContractService.getDecimals());
        //         this._web3ContractService.transferTokens(userEthAddress, tokens).then(response => {
        //           if (response !== undefined) {
        //             var index = this.rowsFilter.findIndex(function (element) {
        //               return element._id == id;
        //             });
        //             this.rowsFilter[index].isTokenSent = true;
        //             this.rowsFilter[index].TokenSentHash = response;
        //             this._userService.updateTokenStateOfUser(response, userDetailTokensToSent.Email).subscribe(service_response => {
        //               console.log(service_response);
        //             }, err => {
        //               var obj = JSON.parse(err._body)
        //               if (obj.code == 404) {
        //                 localStorage.clear();
        //                 this.router.navigateByUrl('/');
        //               }
        //             });
        //           }
        //           else {
        //             alert("Transaction Failed");
        //           }
        //         }).catch(err => {
        //           alert("Error from Metamask");
        //         });
        //       }).catch(err => {
        //         alert("Error from Metamask");
        //       });
        //     })
        //     .catch(err => {
        //       alert("Error from Metamask");
        //     });
        // }
        // else {
        //   alert("Tokens have already been sent to the user");
        // }
    };
    SubscribersComponent.prototype.viewTransaction = function (id) {
        // var userDetailTokensToSent = this.rowsFilter.find(function (element) {
        //   return element._id == id;
        // });
        // console.log(userDetailTokensToSent);
        // window.open(this._transactionBaseURL + userDetailTokensToSent.TokenSentHash, '_blank');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], SubscribersComponent.prototype, "userListTable", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], SubscribersComponent.prototype, "userDetailTable", void 0);
    SubscribersComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-subscribers',
            template: __webpack_require__("../../../../../src/app/theme/Subscribers/Subscribers.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/Subscribers/Subscribers.component.scss"), __webpack_require__("../../../../../src/assets/icon/icofont/css/icofont.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__web3_contracts_smart_contract_service__["a" /* ContractsService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_4__web3_contracts_smart_contract_service__["a" /* ContractsService */]])
    ], SubscribersComponent);
    return SubscribersComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/Subscribers/Subscribers.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscribersModule", function() { return SubscribersModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Subscribers_routing_module__ = __webpack_require__("../../../../../src/app/theme/Subscribers/Subscribers.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Subscribers_component__ = __webpack_require__("../../../../../src/app/theme/Subscribers/Subscribers.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var SubscribersModule = (function () {
    function SubscribersModule() {
    }
    SubscribersModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__Subscribers_routing_module__["a" /* SubscribersRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__Subscribers_component__["a" /* SubscribersComponent */]]
        })
    ], SubscribersModule);
    return SubscribersModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/Subscribers/Subscribers.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubscribersRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Subscribers_component__ = __webpack_require__("../../../../../src/app/theme/Subscribers/Subscribers.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__Subscribers_component__["a" /* SubscribersComponent */],
        data: {
            title: 'Users',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var SubscribersRoutingModule = (function () {
    function SubscribersRoutingModule() {
    }
    SubscribersRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], SubscribersRoutingModule);
    return SubscribersRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=Subscribers.module.chunk.js.map