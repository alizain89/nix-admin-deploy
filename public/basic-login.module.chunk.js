webpackJsonp(["basic-login.module"],{

/***/ "../../../../../src/app/theme/auth/login/basic-login/basic-login-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasicLoginRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basic_login_component__ = __webpack_require__("../../../../../src/app/theme/auth/login/basic-login/basic-login.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__basic_login_component__["a" /* BasicLoginComponent */],
        data: {
            title: 'Simple Login'
        }
    }
];
var BasicLoginRoutingModule = (function () {
    function BasicLoginRoutingModule() {
    }
    BasicLoginRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */]]
        })
    ], BasicLoginRoutingModule);
    return BasicLoginRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/auth/login/basic-login/basic-login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"snackbar\"></div>\r\n<section class=\"login p-fixed d-flex text-center bg-primary common-img-bg\">\r\n  <!-- Container-fluid starts -->\r\n  <div id=\"login\" class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n        <!-- Authentication card start -->\r\n        <div class=\"login-card card-block auth-body mr-auto ml-auto\">\r\n          <form class=\"md-float-material\">\r\n            <div class=\"text-center\">\r\n              <img src=\"assets/images/logo.png\" class=\"ico-image\" alt=\"Gradient Able\">\r\n            </div>\r\n            <div class=\"auth-box\">\r\n              <div class=\"row m-b-20\">\r\n                <div class=\"col-md-12\">\r\n                  <h3 class=\"text-left txt-primary\" style=\"text-align: center !important;\">Sign In</h3>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n              <div class=\"input-group\">\r\n                <input type=\"email\" [(ngModel)]=\"Email\" name=\"Email\" style=\"text-align: center;\" class=\"form-control\" placeholder=\"Email\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <p id=\"email_error\" class=\"error\">Email is Required</p>\r\n              <p id=\"email_valid_error\" class=\"error\">Email is not valid</p>\r\n              <div class=\"input-group\">\r\n                <input type=\"password\" [(ngModel)]=\"Password\" style=\"text-align: center;\" name=\"Password\" class=\"form-control\" placeholder=\"Password\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <p id=\"password_error\" class=\"error\">Password is Required</p>\r\n\r\n              <div class=\"row m-t-25 text-left\">\r\n                <div class=\"col-12\">\r\n                  <div class=\"checkbox-fade fade-in-primary d-\">\r\n                    <!-- <label>\r\n                      <input type=\"checkbox\" (click)=\"rememberMe()\" value=\"\">\r\n                      <span class=\"cr\">\r\n                        <i class=\"cr-icon fa fa-check txt-primary\"></i>\r\n                      </span>\r\n                      <span class=\"text-inverse\" (click)=\"rememberMe()\">Remember me</span>\r\n                    </label> -->\r\n                  </div>\r\n                  <div class=\"forgot-phone text-right f-right\">\r\n                    <a [routerLink]=\"['/forgotpassword']\" class=\"text-right f-w-600 text-inverse\"> Forgot Password?</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"preloader3 loader-block\" id=\"login-loader\">\r\n                <div class=\"circ1\"></div>\r\n                <div class=\"circ2\"></div>\r\n                <div class=\"circ3\"></div>\r\n                <div class=\"circ4\"></div>\r\n              </div>\r\n              <div class=\"row m-t-30\">\r\n                <div class=\"col-md-3\"></div>\r\n                <div class=\"col-md-6\">\r\n                  <button type=\"button\" (keydown)=\"loginWithEnter($event)\" (click)=\"login()\" class=\"btn btn-primary btn-md btn-block waves-effect text-center m-b-20 btn-div\">Sign in</button>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div id=\"twofa\" class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n        <!-- Authentication card start -->\r\n        <div class=\"login-card card-block auth-body mr-auto ml-auto\">\r\n          <form class=\"md-float-material\">\r\n            <div class=\"text-center\">\r\n              <img src=\"assets/images/logo.png\" class=\"ico-image\" alt=\"Gradient Able\">\r\n            </div>\r\n            <div class=\"auth-box\">\r\n              <div class=\"row m-b-20\">\r\n                <div class=\"col-md-12\">\r\n                  <h3 class=\"text-left txt-primary\" style=\"text-align: center !important\">2 Factor Authentication</h3>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n              <div class=\"input-group\">\r\n                <input type=\"text\" [(ngModel)]=\"TwoFACode\" style=\"text-align: center;\" name=\"TwoFACode\" class=\"form-control\" placeholder=\"2FA Code\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <p id=\"two_fa_code\" class=\"error\">Code is Required</p>\r\n\r\n              <div class=\"preloader3 loader-block\" id=\"two-factor-authentication-loader\">\r\n                <div class=\"circ1\"></div>\r\n                <div class=\"circ2\"></div>\r\n                <div class=\"circ3\"></div>\r\n                <div class=\"circ4\"></div>\r\n              </div>\r\n              <div class=\"row m-t-30\">\r\n                <div class=\"col-md-3\"></div>\r\n                <div class=\"col-md-6\">\r\n                  <button type=\"button\" (keydown)=\"twoFAWithEnter($event)\" (click)=\"twoFA()\" class=\"btn btn-primary btn-md btn-block waves-effect text-center m-b-20 two-fa-div\">Submit</button>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/theme/auth/login/basic-login/basic-login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".error {\n  color: Red;\n  display: none; }\n\n#login-loader {\n  display: none;\n  height: 0px !important; }\n\n#two-factor-authentication-loader {\n  display: none;\n  height: 0px !important; }\n\n#twofa {\n  display: none; }\n\nbody {\n  padding-top: 60px; }\n\n.container-fluid {\n  min-width: 100px; }\n\n::-webkit-input-placeholder {\n  text-align: center; }\n\n:-moz-placeholder {\n  /* Firefox 18- */\n  text-align: center; }\n\n::-moz-placeholder {\n  /* Firefox 19+ */\n  text-align: center; }\n\n:-ms-input-placeholder {\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/auth/login/basic-login/basic-login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasicLoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utility_Toast__ = __webpack_require__("../../../../../src/app/theme/utility/Toast.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery__ = __webpack_require__("../../../../jquery/dist/jquery.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BasicLoginComponent = (function () {
    function BasicLoginComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.Email = "";
        this.Password = "";
        this.TwoFACode = "";
        this.RememberMe = false;
        this.userId = 0;
        this.token = "";
        this.toast = new __WEBPACK_IMPORTED_MODULE_2__utility_Toast__["a" /* Toast */]();
    }
    BasicLoginComponent.prototype.ngOnInit = function () {
        var user_obj = localStorage.getItem('user');
        if (user_obj) {
            this.router.navigateByUrl('/admin/dashboard');
        }
        var remember_me = localStorage.getItem("RememberMe");
        if (remember_me == "true") {
            this.Email = localStorage.getItem("Email");
            this.Password = localStorage.getItem("Password");
            this.login();
        }
        else {
            this.Email = "";
            this.Password = "";
        }
    };
    BasicLoginComponent.prototype.rememberMe = function () {
        if (this.RememberMe == false) {
            this.RememberMe = true;
            localStorage.setItem("RememberMe", "true");
            localStorage.setItem("Email", this.Email);
            localStorage.setItem("Password", this.Password);
        }
        else {
            this.RememberMe = false;
        }
    };
    BasicLoginComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    BasicLoginComponent.prototype.loginWithEnter = function (event) {
        if (event.keyCode == 13) {
            this.login();
        }
    };
    BasicLoginComponent.prototype.login = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3_jquery__('.error').hide();
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').hide();
        if (this.Email === "" || this.Email == undefined || this.Email.trim() == "" || this.Email == null) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#email_error').show();
            return;
        }
        if (!this.validateEmail(this.Email)) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#email_valid_error').show();
            return;
        }
        if (this.Password === "" || this.Password == undefined || this.Password.trim() == "" || this.Password == null) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#password_error').show();
            return;
        }
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').show();
        __WEBPACK_IMPORTED_MODULE_3_jquery__(".btn-div").hide();
        this._userService.loginUser(this.Email, this.Password).subscribe(function (response) {
            if (response.code == 200) {
                _this.userId = response.data.user.userID;
                localStorage.setItem('user', JSON.stringify(response.data));
                localStorage.setItem('userObject', JSON.stringify(response.data));
                _this.token = response.data.token;
                console.log(response);
                if (response.data.user.twoFAStatus == 0) {
                    _this._userService.getConfigValues(_this.token).subscribe(function (response) {
                        console.log(response);
                        localStorage.setItem("contract_attributes", JSON.stringify(response.data));
                        _this.router.navigateByUrl('/admin/dashboard');
                    });
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_3_jquery__('#loader').show();
                    __WEBPACK_IMPORTED_MODULE_3_jquery__('#twofa').show();
                    __WEBPACK_IMPORTED_MODULE_3_jquery__('#login').hide();
                }
            }
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').hide();
            var obj = JSON.parse(err._body);
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#snackbar").html(obj.message);
            _this.toast.showToast();
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#login-loader").hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".btn-div").show();
        });
    };
    BasicLoginComponent.prototype.twoFAWithEnter = function (event) {
        if (event.keyCode == 13) {
            this.twoFA();
        }
    };
    BasicLoginComponent.prototype.twoFA = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3_jquery__('.error').hide();
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#loader').hide();
        if (this.TwoFACode === "" || this.TwoFACode == undefined || this.TwoFACode.trim() == "" || this.TwoFACode == null) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#two_fa_code').show();
            return;
        }
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#two-factor-authentication-loader').show();
        __WEBPACK_IMPORTED_MODULE_3_jquery__(".two-fa-div").hide();
        this._userService.verify2FA(this.TwoFACode, this.Email).subscribe(function (response) {
            if (response.code == 200) {
                __WEBPACK_IMPORTED_MODULE_3_jquery__('#loader').show();
                _this._userService.getConfigValues(_this.token).subscribe(function (response) {
                    console.log(response);
                    localStorage.setItem("contract_attributes", JSON.stringify(response.data));
                    _this.router.navigateByUrl('/admin/dashboard');
                });
                console.log(response);
            }
        }, function (err) {
            var obj = JSON.parse(err._body);
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#snackbar").html(obj.message);
            _this.toast.showToast();
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#two-factor-authentication-loader").hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".two-fa-div").show();
        });
    };
    BasicLoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-basic-login',
            template: __webpack_require__("../../../../../src/app/theme/auth/login/basic-login/basic-login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/auth/login/basic-login/basic-login.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["f" /* Router */]])
    ], BasicLoginComponent);
    return BasicLoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/auth/login/basic-login/basic-login.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasicLoginModule", function() { return BasicLoginModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__basic_login_component__ = __webpack_require__("../../../../../src/app/theme/auth/login/basic-login/basic-login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__basic_login_routing_module__ = __webpack_require__("../../../../../src/app/theme/auth/login/basic-login/basic-login-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var BasicLoginModule = (function () {
    function BasicLoginModule() {
    }
    BasicLoginModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__basic_login_routing_module__["a" /* BasicLoginRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__basic_login_component__["a" /* BasicLoginComponent */]]
        })
    ], BasicLoginModule);
    return BasicLoginModule;
}());



/***/ })

});
//# sourceMappingURL=basic-login.module.chunk.js.map