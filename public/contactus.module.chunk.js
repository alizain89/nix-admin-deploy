webpackJsonp(["contactus.module"],{

/***/ "../../../../../src/app/theme/contactus/contactus.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"userList\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n\r\n      <app-card [title]=\"'Support Ticket List'\" [cardOptionBlock]=\"true\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12\">\r\n            <label class=\"dt-cust-search f-right\">\r\n              <div class=\"form-group\">\r\n                <label>Search: </label>\r\n                <input type='text' class=\"form-control input-sm m-l-10\" placeholder='Search Email' (keyup)='updateFilter($event)' />\r\n              </div>\r\n            </label>\r\n          </div>\r\n        </div>\r\n        <ngx-datatable #userListTable class='data-table' [loadingIndicator]=\"loadingIndicator\" [scrollbarH]=\"true\" [columns]=\"columns\"\r\n          [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"50\" [limit]=\"10\" [rows]='rowsFilter'\r\n          (select)='onSelect($event)'>\r\n          <ngx-datatable-column name=\"First Name\" sortable=\"false\" prop=\"FirstName\" style=\"width:40%\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n              {{value}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Email\" sortable=\"false\" prop=\"Email\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n              {{value}}\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Ticket Status\" sortable=\"false\" prop=\"TicketStatus\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                <p *ngIf=\"value == 0\">Pending</p>\r\n                <p *ngIf=\"value != 0\">Completed</p>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"View Detail\" sortable=\"false\" prop=\"_id\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n              <button class=\"view-btn btn btn-primary view-detail-btn\" (click)=\"view(value);settingsModal.show()\">\r\n                View Detail\r\n              </button>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Update\" sortable=\"false\" prop=\"_id\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n              <button class=\"view-btn btn btn-primary view-detail-btn\" (click)=\"closeTicket(value)\">\r\n                Close Ticket\r\n              </button>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n          <ngx-datatable-column name=\"Response\" sortable=\"false\" prop=\"Email\">\r\n            <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n              <button class=\"view-btn btn btn-primary view-detail-btn\" (click)=\"reply(value)\">\r\n                Reply\r\n              </button>\r\n            </ng-template>\r\n          </ngx-datatable-column>\r\n        </ngx-datatable>\r\n      </app-card>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<app-modal-basic #settingsModal [dialogClass]=\"'modal-md'\">\r\n  <div class=\"app-modal-header\">\r\n    <h4 class=\"modal-title\">Ticket Detail</h4>\r\n    <button type=\"button\" class=\"close basic-close\" (click)=\"settingsModal.hide()\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"app-modal-body\">\r\n    <div class=\"form-group row\">\r\n      <div class=\"col-sm-6\">\r\n        <label>First Name</label>\r\n        <p>{{firstName}}</p>\r\n      </div>\r\n      <div class=\"col-sm-6\">\r\n        <label>Last Name</label>\r\n        <p>{{lastName}}</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group row\">\r\n      <div class=\"col-sm-12\">\r\n        <label>Email</label>\r\n        <p>{{email}}</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"form-group row\">\r\n      <div class=\"col-sm-12\">\r\n        <label>Message</label>\r\n        <p>{{message}}</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"app-modal-footer\">\r\n    <button id=\"close-btn\" type=\"button\" class=\"btn btn-default waves-effect\" (click)=\"settingsModal.hide()\">Close</button>\r\n  </div>\r\n</app-modal-basic>"

/***/ }),

/***/ "../../../../../src/app/theme/contactus/contactus.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#detail {\n  display: none;\n  width: 100%; }\n\n#kycdetail {\n  display: none;\n  width: 100%; }\n\n#referraldetail {\n  display: none;\n  width: 100%; }\n\n.go-back {\n  margin-bottom: 15px; }\n\n.datatable-body-cell-label {\n  margin-top: -10px !important; }\n\n.view-detail-btn {\n  font-size: 15px;\n  font-weight: 500;\n  width: 100%;\n  height: 44px;\n  margin-top: -10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/contactus/contactus.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUSComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactUSComponent = (function () {
    function ContactUSComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.userDetail = [];
        this.userKYCStatus = "";
        this.passportImageURL = "";
        this.passportSelfieImageURL = "";
    }
    ContactUSComponent.prototype.ngOnInit = function () {
        this.getUserList();
    };
    ContactUSComponent.prototype.getUserList = function () {
        var _this = this;
        $('#userList').show();
        this._userService.getSupportList().subscribe(function (res) {
            _this.rowsFilter = res.message;
            _this.tempFilter = res.message;
            console.log(res);
        });
    };
    ContactUSComponent.prototype.view = function (id) {
        var obj = this.rowsFilter.find(function (x) { return x._id == id; });
        this.firstName = obj.FirstName;
        this.lastName = obj.LastName;
        this.email = obj.Email;
        this.message = obj.Message;
    };
    ContactUSComponent.prototype.reply = function (email) {
        window.location.href = "mailto:" + email;
    };
    ContactUSComponent.prototype.closeTicket = function (id) {
        var _this = this;
        this._userService.closeTicket(id).subscribe(function (response) {
            for (var i = 0; i < _this.rowsFilter.length; i++) {
                if (_this.rowsFilter[i]._id == id) {
                    _this.rowsFilter[i].TicketStatus = 1;
                }
            }
        });
    };
    ContactUSComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        // filter our data
        var temp = this.tempFilter.filter(function (d) {
            return d.Email.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rowsFilter = temp;
        // Whenever the filter changes, always go back to the first page
        this.userListTable.offset = 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], ContactUSComponent.prototype, "userListTable", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], ContactUSComponent.prototype, "userDetailTable", void 0);
    ContactUSComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contactus',
            template: __webpack_require__("../../../../../src/app/theme/contactus/contactus.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/contactus/contactus.component.scss"), __webpack_require__("../../../../../src/assets/icon/icofont/css/icofont.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */]])
    ], ContactUSComponent);
    return ContactUSComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/contactus/contactus.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUSModule", function() { return ContactUSModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__contactus_routing_module__ = __webpack_require__("../../../../../src/app/theme/contactus/contactus.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contactus_component__ = __webpack_require__("../../../../../src/app/theme/contactus/contactus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ContactUSModule = (function () {
    function ContactUSModule() {
    }
    ContactUSModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__contactus_routing_module__["a" /* ContactUSRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__contactus_component__["a" /* ContactUSComponent */]]
        })
    ], ContactUSModule);
    return ContactUSModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/contactus/contactus.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUSRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__contactus_component__ = __webpack_require__("../../../../../src/app/theme/contactus/contactus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__contactus_component__["a" /* ContactUSComponent */],
        data: {
            title: 'Users',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var ContactUSRoutingModule = (function () {
    function ContactUSRoutingModule() {
    }
    ContactUSRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], ContactUSRoutingModule);
    return ContactUSRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=contactus.module.chunk.js.map