webpackJsonp(["contract.module"],{

/***/ "../../../../../src/app/theme/contract/contract-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contract_component__ = __webpack_require__("../../../../../src/app/theme/contract/contract.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__contract_component__["a" /* ContractComponent */],
        data: {
            title: 'Default',
            icon: 'icon-home',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit',
            status: false
        }
    }
];
var ContractRoutingModule = (function () {
    function ContractRoutingModule() {
    }
    ContractRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */]]
        })
    ], ContractRoutingModule);
    return ContractRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/contract/contract.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"error-div\" class=\"row\" style=\"display: none\">\r\n    <div class=\"col-md-12 block\">\r\n        <p class=\"generic-error\">{{DAPPError}}</p>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"working-div\" class=\"row\">\r\n    <div class=\"col-md-3\"></div>\r\n    <div class=\"col-md-6 block\">\r\n            <app-card [title]=\"'Rate'\" [cardOptionBlock]=\"true\">\r\n                    <div class=\"form-group row\">\r\n                            <p class=\"sideBoxTxt\">1 ETH = {{calcTokenData?.EthRate}} USD</p>\r\n                            <p class=\"sideBoxTxt\">1 ETH = {{calcTokenData?.KrwRate}} KRW</p>\r\n                            \r\n                        </div>\r\n                </app-card>\r\n        <app-card [title]=\"'Private Sale'\" [cardOptionBlock]=\"true\">\r\n            <div class=\"form-group row\">\r\n                <label class=\"col-sm-4 col-form-label\">ETH Address</label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" [(ngModel)]=\"WalletAddress\" min=\"0\" name=\"TransactionFeePercentage\" class=\"form-control\">\r\n                    <div class=\"messages text-danger\" id=\"wallet_id_error\">Wallet Address is Required</div>\r\n                    <div class=\"messages text-danger\" id=\"wallet_invalid_error\">Wallet Address is not Valid</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group row\">\r\n                <label class=\"col-sm-4 col-form-label\">ETH</label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" [(ngModel)]=\"TokenAmount\" min=\"0\" name=\"TransactionFeePercentage\" class=\"form-control\">\r\n                    <div class=\"messages text-danger\" id=\"tokens_id_error\">USD Amount is Required</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group row\">\r\n                <label class=\"col-sm-4 col-form-label\">Bonus %</label>\r\n                <div class=\"col-sm-8\">\r\n                    <input type=\"text\" [(ngModel)]=\"Bonus\" min=\"0\" id=\"bonusinput\" (keyup)=\"bonusChck($event.target.value)\" name=\"TransactionFeePercentage\" class=\"form-control\">\r\n                    <div class=\"messages text-danger\" id=\"bonus_error\">Bonus is Required</div>\r\n                </div>\r\n            </div>\r\n            <div id=\"categories-add2\" class=\"form-group row fetcher\">\r\n                <a href=\"{{success}}\" style=\"text-decoration: underline;\" target=\"blank\">View Transaction Link</a>\r\n            </div>\r\n            <div class=\"preloader3 loader-block\" id=\"two-factor-authentication-loader\">\r\n                <div class=\"circ1\"></div>\r\n                <div class=\"circ2\"></div>\r\n                <div class=\"circ3\"></div>\r\n                <div class=\"circ4\"></div>\r\n            </div>\r\n            <div class=\"form-group row\">\r\n                <label class=\"col-sm-4\"></label>\r\n                <div class=\"col-sm-8\">\r\n                    <button type=\"submit\" class=\"btn btn-primary m-b-0 btn-div\" (click)=\"submitPrivateSale()\">Calculate</button>\r\n                </div>\r\n            </div>\r\n        </app-card>\r\n    </div>\r\n    <div class=\"col-md-3\">\r\n          \r\n\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<app-modal-basic #modalSmall [dialogClass]=\"'modal-lg'\">\r\n    <div class=\"app-modal-header\">\r\n        <h4 class=\"modal-title\">Alert</h4>\r\n        <button type=\"button\" class=\"close basic-close\" (click)=\"modalSmall.hide();closeModal()\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"app-modal-body\">\r\n        <div id=\"main-div\" style=\"display:none\">\r\n            <form #EditForm=\"ngForm\">\r\n                <div style=\"padding-left: 40%;\" class=\"form-group row\">\r\n                    <ul>\r\n                        <li>No. of Transactions : {{no_of_transactions}}</li>\r\n                        <li>Eth Balance : {{remaining_eth_balance}}</li>\r\n                        <li>Estimated Transaction Cost : {{estimated_gas_price}}</li>\r\n                    </ul>\r\n                </div>\r\n                <div id=\"btns-div\" style=\"padding-left: 40%;\" class=\"form-group row\">\r\n                    <button type=\"submit\" class=\"btn btn-primary m-b-0\" (click)=\"initiateTransaction()\">Continue ({{no_of_transactions}})</button>\r\n                    <button type=\"button\" class=\"btn btn-default waves-effect\" (click)=\"modalSmall.hide();closeModal()\">Close</button>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</app-modal-basic>\r\n<div id=\"snackbar\"></div>\r\n\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/theme/contract/contract.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContractComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/services/smart.contract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__web3_contracts_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/web3/contracts/smart.contract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery__ = __webpack_require__("../../../../jquery/dist/jquery.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_takeWhile__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/takeWhile.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ContractComponent = (function () {
    function ContractComponent(_smartContractService, _userService, web3_smart_contract_service, router) {
        var _this = this;
        this._smartContractService = _smartContractService;
        this._userService = _userService;
        this.web3_smart_contract_service = web3_smart_contract_service;
        this.router = router;
        this.WalletAddress = "";
        this.TokenAmount = 0;
        this.success = "";
        this.UserCount = 0;
        this.error = "";
        this.totalNumberOfUsers = 0;
        this.DAPPError = "";
        this.Bonus = 0;
        this.BonusUpdate = 0;
        this.transaction_link = "";
        this.no_of_transactions = 0;
        this.estimated_gas_price = 0;
        this.remaining_eth_balance = 0;
        this.release_txhash = "";
        this.close_txhash = "";
        this.web3_smart_contract_service.connect().then(function (result) {
        }).catch(function (err) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#working-div').hide();
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#error-div').show();
            _this.DAPPError = err;
        });
    }
    ContractComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = JSON.parse(localStorage.getItem("user"));
        this._smartContractService.getTotalNumberOfUsers().subscribe(function (response) {
            _this.totalNumberOfUsers = response.message;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
        this._userService.getBonus(this.user.token).subscribe(function (response) {
            if (response.data.length > 0) {
                _this.BonusUpdate = response.data[0].Bonus;
            }
            else {
                _this.BonusUpdate = 0;
            }
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
        var self = this;
        if (document.getElementById) {
            window.alert = function (txt) {
                self.createCustomAlert(txt);
            };
        }
        this._smartContractService.calculateTokens(this.user.token, this.TokenAmount, this.Bonus).subscribe(function (response_initial) {
            if (response_initial.code == 200) {
                console.log(response_initial);
                _this.calcTokenData = response_initial.data;
            }
        });
    };
    ContractComponent.prototype.createCustomAlert = function (txt) {
        var d = document;
        if (d.getElementById("modalContainer"))
            return;
        var mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";
        var alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if (d.all && !window)
            alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
        alertObj.style.visibility = "visible";
        var h1 = alertObj.appendChild(d.createElement("h1"));
        h1.appendChild(d.createTextNode("Ooops!"));
        var msg = alertObj.appendChild(d.createElement("p"));
        msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;
        var btn = alertObj.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode("Ok"));
        btn.href = "#";
        btn.focus();
        var self = this;
        btn.onclick = function () { self.removeCustomAlert(); return false; };
        alertObj.style.display = "block";
    };
    ContractComponent.prototype.removeCustomAlert = function () {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    };
    ContractComponent.prototype.ful = function () {
        alert('Alert this pages');
    };
    ContractComponent.prototype.ngAfterViewInit = function () {
    };
    ContractComponent.prototype.initiateTransaction = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery__('#fetching_categories_message').hide();
        __WEBPACK_IMPORTED_MODULE_5_jquery__('.text-danger').hide();
        __WEBPACK_IMPORTED_MODULE_5_jquery__(".fetcher").hide();
        if (this.UserCount === undefined || this.UserCount <= 0) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#user_count_error').show();
            return;
        }
        if (this.UserCount >= this.totalNumberOfUsers) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#user_count_exceed_error').show();
            return;
        }
        __WEBPACK_IMPORTED_MODULE_5_jquery__("#initiateTransaction-loader").show();
        this._smartContractService.getEstimatedPrice(this.UserCount).subscribe(function (response) {
            if (response.usersEthAddresses !== undefined && response.usersEthAddresses.length > 0) {
                _this.web3_smart_contract_service.initiateTransaction(response.usersEthAddresses, response.usersTokens).then(function (result) {
                    __WEBPACK_IMPORTED_MODULE_5_jquery__("#initiateTransaction-loader").hide();
                    __WEBPACK_IMPORTED_MODULE_5_jquery__("#categories-add3").show();
                    _this.success = _this.web3_smart_contract_service.getTransactionBaseUrl() + result;
                    _this._smartContractService.updateTransactionStatusOfUsers(_this.user.token, response.usersEthAddresses).subscribe(function (response_of_inner_api) {
                    }, function (err) {
                        var obj = JSON.parse(err._body);
                        if (obj.code == 404) {
                            localStorage.clear();
                            _this.router.navigateByUrl('/');
                        }
                    });
                }).catch(function (error) {
                    __WEBPACK_IMPORTED_MODULE_5_jquery__("#initiateTransaction-loader").hide();
                    __WEBPACK_IMPORTED_MODULE_5_jquery__("#categories-add3").hide();
                    alert(error);
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_5_jquery__("#initiateTransaction-loader").hide();
                __WEBPACK_IMPORTED_MODULE_5_jquery__("#categories-add3-error").show();
                _this.error = "No Users Found";
            }
        }, function (err) {
            if (err) {
                __WEBPACK_IMPORTED_MODULE_5_jquery__('#success-div').show();
                __WEBPACK_IMPORTED_MODULE_5_jquery__('.preloader3').hide();
                __WEBPACK_IMPORTED_MODULE_5_jquery__('#btns-div').show();
                var obj = JSON.parse(err._body);
                if (obj.code == 404) {
                    localStorage.clear();
                    _this.router.navigateByUrl('/');
                }
            }
        });
    };
    ContractComponent.prototype.submitPrivateSale = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery__('#fetching_categories_message').hide();
        __WEBPACK_IMPORTED_MODULE_5_jquery__('.text-danger').hide();
        __WEBPACK_IMPORTED_MODULE_5_jquery__(".fetcher").hide();
        if (this.WalletAddress === undefined || this.WalletAddress == "" || this.WalletAddress.trim() == "") {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#wallet_id_error').show();
            return;
        }
        if (this.TokenAmount === undefined || this.TokenAmount <= 0) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#tokens_id_error').show();
            return;
        }
        if (this.Bonus === undefined || this.Bonus <= 0) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#bonus_error').show();
            return;
        }
        this.web3_smart_contract_service.isValidWalletAddress(this.WalletAddress)
            .then(function (isValidAddress) {
            if (isValidAddress == true) {
                __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").show();
                __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").hide();
                _this._smartContractService.calculateTokens(_this.user.token, _this.TokenAmount, _this.Bonus).subscribe(function (response_initial) {
                    // Check Eth balance and Tokens
                    if (response_initial.code == 200) {
                        var a = response_initial.data.bonusTokens;
                        var b = response_initial.data.totalTokensWithoutBonus;
                        var c = parseFloat(a) + parseFloat(b);
                        __WEBPACK_IMPORTED_MODULE_4_sweetalert2___default()({
                            title: 'Token Rate Breakdown',
                            html: "<p style='font-weight:bold;'>Bonus Tokens(" + _this.Bonusvalue + "%): &nbsp;&nbsp;" + response_initial.data.bonusTokens + "</p><p style='font-weight:bold;>Token Rate: &nbsp;&nbsp;  " + response_initial.data.tokenrate + "</p>" + "</p><p style='font-weight:bold;'>Number of Tokens:  &nbsp;&nbsp; " + response_initial.data.totalTokensWithoutBonus + "</p><p style='font-weight:bold;'>Total Tokens:  &nbsp;&nbsp; " + c.toFixed(2) + "</p>",
                            showCancelButton: true,
                            confirmButtonText: 'Submit',
                            cancelButtonText: 'Cancel'
                        }).then(function (result) {
                            if (result.value) {
                                _this.web3_smart_contract_service.getEthereumBalance()
                                    .then(function (eth_balance) {
                                    if (eth_balance <= 0) {
                                        alert("You dont have enough tokens to transfer");
                                        return;
                                    }
                                    _this.web3_smart_contract_service.getTokenBalance()
                                        .then(function (token_balance) {
                                        if (token_balance <= 0) {
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                            alert("You dont have enough tokens to transfer");
                                            return;
                                        }
                                        if (token_balance > response_initial.data) {
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                            alert("Insufficient Token Balance. You have " + token_balance + " tokens");
                                            return;
                                        }
                                        var tokens = parseFloat(response_initial.data.totalTokensWithoutBonus) * Math.pow(10, _this.web3_smart_contract_service.getDecimals());
                                        var bonusTokens = parseFloat(response_initial.data.bonusTokens) * Math.pow(10, _this.web3_smart_contract_service.getDecimals());
                                        _this.web3_smart_contract_service.transferTokens(_this.WalletAddress, tokens).then(function (res) {
                                            var tx_hash = res;
                                            _this._smartContractService.privateSale(_this.user.token, _this.WalletAddress, _this.TokenAmount, _this.Bonus, res).subscribe(function (res) {
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__("#success-div").show();
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__(".fetcher").show();
                                                if (res.code == 200) {
                                                    __WEBPACK_IMPORTED_MODULE_5_jquery__(".two-fa-div").hide();
                                                    _this.WalletAddress = "";
                                                    _this.success = _this.web3_smart_contract_service.getTransactionBaseUrl() + tx_hash;
                                                    _this.TokenAmount = 0;
                                                    _this.Bonus = 0;
                                                    __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                                    __WEBPACK_IMPORTED_MODULE_5_jquery__('#fetching_categories_message').show();
                                                }
                                            }, function (err) {
                                                var obj = JSON.parse(err._body);
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__("#snackbar").html(obj.message);
                                                _this.toast.showToast();
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                                __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                                var obj = JSON.parse(err._body);
                                                if (obj.code == 404) {
                                                    localStorage.clear();
                                                    _this.router.navigateByUrl('/');
                                                }
                                            });
                                        }).catch(function (err) {
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                            __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                            alert("Error from Metamask");
                                        });
                                    })
                                        .catch(function (token_balance_err) {
                                        __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                        __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                        alert("Error from Metamask");
                                    });
                                })
                                    .catch(function (ethereum_balance_error) {
                                    __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                                    __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                                    alert("Error from Metamask");
                                });
                            }
                        });
                        __WEBPACK_IMPORTED_MODULE_5_jquery__("#two-factor-authentication-loader").hide();
                        __WEBPACK_IMPORTED_MODULE_5_jquery__(".btn-div").show();
                    }
                }, function (err) {
                    var obj = JSON.parse(err._body);
                    if (obj.code == 404) {
                        localStorage.clear();
                        _this.router.navigateByUrl('/');
                    }
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_5_jquery__('#wallet_invalid_error').show();
            }
        })
            .catch(function (err) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#wallet_invalid_error').show();
        });
    };
    ContractComponent.prototype.bonusChck = function (value) {
        if (value > 100) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__("#bonusinput").val('');
        }
        else {
            this.Bonusvalue = value;
        }
    };
    ContractComponent.prototype.updateBonus = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery__('#bonus_error').hide();
        if (this.BonusUpdate <= 0) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#bonus_error').show();
            return;
        }
        this._userService.updateBonus(this.user.token, this.BonusUpdate).subscribe(function (response) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__('#success-response').show();
            setTimeout(function () {
                __WEBPACK_IMPORTED_MODULE_5_jquery__('#success-response').hide();
            }, 5000);
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    ContractComponent.prototype.Close = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery__("#close-loader").show();
        this.web3_smart_contract_service.closeCrowdSale().then(function (tx_hash) {
            _this.close_txhash = tx_hash;
            __WEBPACK_IMPORTED_MODULE_5_jquery__("#close-loader").hide();
        }).catch(function (error) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__("#close-loader").hide();
            alert("Error from Metamask");
        });
    };
    ContractComponent.prototype.Release = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery__("#release-loader").show();
        this.web3_smart_contract_service.release().then(function (tx_hash) {
            _this.release_txhash = tx_hash;
            __WEBPACK_IMPORTED_MODULE_5_jquery__("#release-loader").hide();
        }).catch(function (error) {
            __WEBPACK_IMPORTED_MODULE_5_jquery__("#release-loader").hide();
            alert("Error from Metamask");
        });
    };
    ContractComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contract',
            template: __webpack_require__("../../../../../src/app/theme/contract/contract.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/contract/contract.style.scss"), __webpack_require__("../../../../../src/assets/icon/icofont/css/icofont.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_smart_contract_service__["a" /* SmartContractService */], __WEBPACK_IMPORTED_MODULE_3__web3_contracts_smart_contract_service__["a" /* ContractsService */]],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_smart_contract_service__["a" /* SmartContractService */], __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__web3_contracts_smart_contract_service__["a" /* ContractsService */], __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */]])
    ], ContractComponent);
    return ContractComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/contract/contract.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContractModule", function() { return ContractModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contract_component__ = __webpack_require__("../../../../../src/app/theme/contract/contract.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contract_routing_module__ = __webpack_require__("../../../../../src/app/theme/contract/contract-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_chartjs__ = __webpack_require__("../../../../angular2-chartjs/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular2_chartjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angular2_chartjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ContractModule = (function () {
    function ContractModule() {
    }
    ContractModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_3__contract_routing_module__["a" /* ContractRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5_angular2_chartjs__["ChartModule"],
                __WEBPACK_IMPORTED_MODULE_7__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__["SimpleNotificationsModule"].forRoot()
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__contract_component__["a" /* ContractComponent */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__contract_component__["a" /* ContractComponent */]]
        })
    ], ContractModule);
    return ContractModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/contract/contract.style.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#seo-card1 canvas,\n#seo-card2 canvas {\n  height: 150px !important;\n  border-bottom-right-radius: 5px;\n  border-bottom-left-radius: 5px; }\n  @media only screen and (max-width: 768px) {\n    #seo-card1 canvas,\n    #seo-card2 canvas {\n      padding-bottom: 30px; } }\n  .text-danger {\n  display: none; }\n  .fetcher {\n  padding-left: 5%;\n  display: none; }\n  #two-factor-authentication-loader {\n  display: none;\n  height: 0px !important; }\n  #initiateTransaction-loader {\n  display: none;\n  height: 0px !important; }\n  .generic-error {\n  color: red;\n  font-size: 20px;\n  text-align: center; }\n  .preloader3 {\n  display: none;\n  height: 0px !important; }\n  #success-response {\n  display: none; }\n  #swal2-content {\n  display: block;\n  width: 60%;\n  margin: 0 auto; }\n  #swal2-content p {\n  text-align: left; }\n  .card .card-header {\n  padding: 15px; }\n  .sideBoxTxt {\n  width: 100%;\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ })

});
//# sourceMappingURL=contract.module.chunk.js.map