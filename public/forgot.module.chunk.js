webpackJsonp(["forgot.module"],{

/***/ "../../../../../src/app/theme/auth/forgot/forgot-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgot_component__ = __webpack_require__("../../../../../src/app/theme/auth/forgot/forgot.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__forgot_component__["a" /* ForgotComponent */],
        data: {
            title: 'Forgot'
        }
    }
];
var ForgotRoutingModule = (function () {
    function ForgotRoutingModule() {
    }
    ForgotRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */]]
        })
    ], ForgotRoutingModule);
    return ForgotRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/auth/forgot/forgot.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"snackbar\"></div>\r\n<section class=\"login p-fixed d-flex text-center bg-primary common-img-bg\">\r\n  <!-- Container-fluid starts -->\r\n  <div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n        <!-- Authentication card start -->\r\n        <div class=\"login-card card-block auth-body mr-auto ml-auto\">\r\n          <form class=\"md-float-material\">\r\n            <div class=\"text-center\">\r\n              <img src=\"assets/images/logo.png\" class=\"ico-image\" alt=\"logo.png\">\r\n            </div>\r\n            <div class=\"auth-box\">\r\n              <div class=\"row m-b-20\">\r\n                <div class=\"col-md-12\">\r\n                  <h3 class=\"text-left\" style=\"text-align: center !important\">Recover Your Password</h3>\r\n                </div>\r\n              </div>\r\n              <p class=\"text-inverse b-b-default text-right\" style=\"text-align: center !important;\">Back to\r\n                <a [routerLink]=\"['/']\" style=\"text-decoration: underline;\">Login</a>\r\n              </p>\r\n              <div class=\"input-group\">\r\n                <input type=\"email\" [(ngModel)]=\"Email\" style=\"text-align: center;\" name=\"Email\" class=\"form-control\" placeholder=\"Email\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <p id=\"email_error\" class=\"error\">Email is Required</p>\r\n              <p id=\"email_valid_error\" class=\"error\">Email is not valid</p>\r\n              <div class=\"preloader3 loader-block\" id=\"login-loader\">\r\n                <div class=\"circ1\"></div>\r\n                <div class=\"circ2\"></div>\r\n                <div class=\"circ3\"></div>\r\n                <div class=\"circ4\"></div>\r\n              </div>\r\n              <div class=\"row\">\r\n                  <div class=\"col-md-3\"></div>\r\n\r\n                <div class=\"col-md-6\">\r\n                  <button type=\"button\" class=\"btn btn-primary btn-md btn-block waves-effect text-center m-b-20\" (click)=\"forgetPassword()\">Reset Password</button>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n            </div>\r\n          </form>\r\n          <!-- end of form -->\r\n        </div>\r\n        <div class=\"login-card forgot-password-confirm card-block auth-body mr-auto ml-auto\">\r\n          <form class=\"md-float-material\">\r\n            <div class=\"text-center\">\r\n\r\n              <img src=\"assets/images/logo.png\" alt=\"Amicorum\">\r\n            </div>\r\n            <div class=\"auth-box\">\r\n              <div class=\"row m-b-20\">\r\n                <div class=\"col-md-12\">\r\n                  <h3 class=\"my-head uppercase no-bottom\">Forgot Password</h3>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n\r\n              <div class=\"input-group\">\r\n                <input type=\"text\" id=\"twoFaInput1\" class=\"form-control\" style=\"text-align: center;\" name=\"twoFaInput\" [(ngModel)]=\"Code\" placeholder=\"Enter your Code\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <small id=\"code-password-error\" class=\"text-danger \">\r\n                Code can't be blank\r\n              </small>\r\n              <div class=\"input-group\">\r\n                <input type=\"password\" id=\"twoFaInput2\" class=\"form-control\" style=\"text-align: center;\" name=\"twoFaInput2\" [(ngModel)]=\"Password\" placeholder=\"Enter your New Password\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <small id=\"new-password-error\" class=\"text-danger \">\r\n                New Password can't be blank\r\n              </small>\r\n              <div class=\"input-group\">\r\n                <input type=\"password\" id=\"twoFaInput3\" class=\"form-control\" style=\"text-align: center;\" name=\"twoFaInput3\" [(ngModel)]=\"ConfirmPassword\" placeholder=\"Enter your Confirm Password\">\r\n                <span class=\"md-line\"></span>\r\n              </div>\r\n              <small id=\"confirm-password-error\" class=\"text-danger \">\r\n                Confirm Password can't be blank\r\n              </small>\r\n              <small id=\"confirm-password-password-error\" class=\"text-danger \">\r\n                Password and Confirm Password are not same\r\n              </small>\r\n              <div class=\"preloader3 loader-block forgot-password-loader\">\r\n                <div _ngcontent-c10=\"\" class=\"circ1\"></div>\r\n                <div _ngcontent-c10=\"\" class=\"circ2\"></div>\r\n                <div _ngcontent-c10=\"\" class=\"circ3\"></div>\r\n                <div _ngcontent-c10=\"\" class=\"circ4\"></div>\r\n              </div>\r\n\r\n              <div class=\"row m-t-30\">\r\n                <div class=\"col-md-12 forgot-password-btn-div\">\r\n                  <button type=\"button\" class=\"btn btn-primary btn-md btn-block waves-effect text-center m-b-20\" (click)=\"verifyForgotPassword()\">Next</button>\r\n                </div>\r\n              </div>\r\n              <div class=\"row m-t-25 text-left\">\r\n                <div class=\"col-12\">\r\n                  <div class=\"forgot-phone text-right f-right\">\r\n                    <a style=\"cursor:pointer\" [routerLink]=\"['/']\" class=\"text-right f-w-600 text-inverse\">Back to Login</a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <hr/>\r\n            </div>\r\n          </form>\r\n          <!-- end of form -->\r\n        </div>\r\n        <!-- Authentication card end -->\r\n      </div>\r\n      <!-- end of col-sm-12 -->\r\n    </div>\r\n    <!-- end of row -->\r\n  </div>\r\n  <!-- end of container-fluid -->\r\n\r\n</section>"

/***/ }),

/***/ "../../../../../src/app/theme/auth/forgot/forgot.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".error {\n  color: Red;\n  display: none; }\n\n#login-loader {\n  display: none;\n  height: 0px !important; }\n\n#two-factor-authentication-loader {\n  display: none;\n  height: 10px !important; }\n\n#twofa {\n  display: none; }\n\n.forgot-password-confirm {\n  display: none; }\n\n.text-danger {\n  display: none; }\n\n.forgot-password-loader {\n  display: none; }\n\n::-webkit-input-placeholder {\n  text-align: center; }\n\n:-moz-placeholder {\n  /* Firefox 18- */\n  text-align: center; }\n\n::-moz-placeholder {\n  /* Firefox 19+ */\n  text-align: center; }\n\n:-ms-input-placeholder {\n  text-align: center; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/auth/forgot/forgot.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utility_Toast__ = __webpack_require__("../../../../../src/app/theme/utility/Toast.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery__ = __webpack_require__("../../../../jquery/dist/jquery.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ForgotComponent = (function () {
    function ForgotComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.Email = "";
        this.Code = "";
        this.Password = "";
        this.ConfirmPassword = "";
        this.toast = new __WEBPACK_IMPORTED_MODULE_2__utility_Toast__["a" /* Toast */]();
        this.user = JSON.parse(localStorage.getItem("user"));
    }
    ForgotComponent.prototype.ngOnInit = function () {
        var user_obj = localStorage.getItem('user');
        if (user_obj) {
            this.router.navigateByUrl('/admin/dashboard');
        }
    };
    ForgotComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    ForgotComponent.prototype.forgetPassword = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3_jquery__('.error').hide();
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').hide();
        if (this.Email === "" || this.Email == undefined || this.Email.trim() == "" || this.Email == null) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#email_error').show();
            return;
        }
        if (!this.validateEmail(this.Email)) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#email_valid_error').show();
            return;
        }
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').show();
        this._userService.forgotPassword(this.Email).subscribe(function (response) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#snackbar").html(response.message);
            _this.toast.showToast();
            var self = _this;
            setTimeout(function () {
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.login-card').hide();
                __WEBPACK_IMPORTED_MODULE_3_jquery__('.forgot-password-confirm').show();
            }, 5000);
            console.log(response);
        }, function (err) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#login-loader').hide();
            var obj = JSON.parse(err._body);
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#snackbar").html(obj.message);
            _this.toast.showToast();
            __WEBPACK_IMPORTED_MODULE_3_jquery__("#login-loader").hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".btn-div").show();
        });
    };
    ForgotComponent.prototype.verifyForgotPassword = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3_jquery__('.text-danger').hide();
        __WEBPACK_IMPORTED_MODULE_3_jquery__('#error').hide();
        if (this.Code === undefined || this.Code == null || this.Code == "" || this.Code.trim() == "") {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#code-password-error').show();
            return;
        }
        if (this.Password === undefined || this.Password == null || this.Password == "" || this.Password.trim() == "") {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#new-password-error').show();
            return;
        }
        if (this.ConfirmPassword === undefined || this.ConfirmPassword == null || this.ConfirmPassword == "" || this.ConfirmPassword.trim() == "") {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#confirm-password-error').show();
            return;
        }
        if (this.ConfirmPassword != this.Password) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__('#confirm-password-password-error').show();
            return;
        }
        __WEBPACK_IMPORTED_MODULE_3_jquery__(".forgot-password-loader").show();
        __WEBPACK_IMPORTED_MODULE_3_jquery__(".forgot-password-btn-div").hide();
        this._userService.verifyForgotPassword(this.Email, this.Code, this.Password).subscribe(function (response) {
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".forgot-password-confirm").hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".forgot-password-loader").hide();
            __WEBPACK_IMPORTED_MODULE_3_jquery__(".forgot-password-btn-div").show();
            _this.router.navigateByUrl('/');
        });
    };
    ForgotComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-forgot',
            template: __webpack_require__("../../../../../src/app/theme/auth/forgot/forgot.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/auth/forgot/forgot.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__angular_router__["f" /* Router */]])
    ], ForgotComponent);
    return ForgotComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/auth/forgot/forgot.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotModule", function() { return ForgotModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forgot_component__ = __webpack_require__("../../../../../src/app/theme/auth/forgot/forgot.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forgot_routing_module__ = __webpack_require__("../../../../../src/app/theme/auth/forgot/forgot-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ForgotModule = (function () {
    function ForgotModule() {
    }
    ForgotModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__forgot_routing_module__["a" /* ForgotRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_4__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__forgot_component__["a" /* ForgotComponent */]]
        })
    ], ForgotModule);
    return ForgotModule;
}());



/***/ })

});
//# sourceMappingURL=forgot.module.chunk.js.map