webpackJsonp(["kyc-detail.module"],{

/***/ "../../../../../src/app/theme/kyc-detail/kyc-detail-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KycDetailRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kyc_detail_component__ = __webpack_require__("../../../../../src/app/theme/kyc-detail/kyc-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_2__kyc_detail_component__["a" /* KycDetailComponent */],
        data: {
            title: 'Default',
            status: false
        }
    }
];
var KycDetailRoutingModule = (function () {
    function KycDetailRoutingModule() {
    }
    KycDetailRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */]]
        })
    ], KycDetailRoutingModule);
    return KycDetailRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/kyc-detail/kyc-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".table th, .table td{\r\n    border-top: unset !important;\r\n}\r\n.styling{\r\n    border: 1px solid #f48928;\r\n    margin-top: 30px;\r\n}\r\n.tbl-head{\r\n   \r\n    text-align: center;\r\n}\r\n.table thead th{\r\n    border-bottom: unset;\r\n    font-size: 22px;\r\n}\r\n.nn-tbl{\r\n    width: 30%;\r\n    margin: 0 auto;\r\n}\r\n.font16{\r\n    font-size: 16px;\r\n}\r\n.cntrAlgn{\r\n    text-align: center;\r\n}\r\n.kycImg{\r\n    height: 220px;\r\n}\r\n.not-sub{\r\n    text-align: center;\r\n    font-size: 20px;\r\n    color: red;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/kyc-detail/kyc-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"snackbar\"></div>\r\n<div class=\"tableDiv\">\r\n        <button class=\"btn btn-primary go-back\" (click)=\"back()\">Go Back</button>\r\n<div class=\"go-back\" style=\"float:right;\" id=\"btnDiv\">\r\n      <button type=\"button\" *ngIf=\"viewingUser.isKycVerified == 1 || viewingUser.isKycVerified == 3  \" class=\"btn btn-outline-success\" id=\"verifyBtn\" (click)=\"VerifyMethod()\">Verify</button>\r\n      <button type=\"button\" *ngIf=\"viewingUser.isKycVerified == 1 || viewingUser.isKycVerified ==2\" class=\"btn btn-outline-danger\" id=\"rejectBtn\" (click)=\"rejectMethod()\">Reject</button>\r\n    </div>\r\n</div>\r\n<div class=\"table-responsive styling\" *ngIf=\"CnicDiv\">\r\n    <table class=\"table nn-tbl\">\r\n        <thead>\r\n            <th colspan=\"2\" class=\"tbl-head\">\r\n                KYC DETAILS\r\n            </th>\r\n        </thead>\r\n        <tbody *ngIf=\"kycSub\">\r\n            \r\n                <tr>\r\n                        <th class=\"algnRyt\">\r\n                            Full Name\r\n                        </th>\r\n                        <td class=\"font16\">\r\n                                {{kycdetails?.FirstName}} {{kycdetails?.LastName}}\r\n                        </td>\r\n                    </tr>\r\n                    \r\n                    <tr>\r\n                            <th class=\"algnRyt\">\r\n                                CNIC Number\r\n                            </th>\r\n                            <td class=\"font16\">\r\n                                    {{kycdetails?.CnicNumber}}\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                                <th class=\"algnRyt\">\r\n                                    Country\r\n                                </th>\r\n                                <td class=\"font16\">\r\n                                        {{kycdetails?.Country}}\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                    <th class=\"algnRyt\">\r\n                                     Date of Birth\r\n                                    </th>\r\n                                    <td class=\"font16\">\r\n                                            {{kycdetails?.Dob}}\r\n                                    </td>\r\n                                </tr>\r\n                                \r\n        </tbody>\r\n        <div class=\"not-sub\" *ngIf=\"kycNotSub\">\r\n            KYC details are not supplied by the user.\r\n        </div>\r\n    </table>\r\n   \r\n</div>\r\n\r\n<div class=\"table-responsive styling\" *ngIf=\"PassDiv\">\r\n    <table class=\"table nn-tbl\">\r\n        <thead>\r\n            <th colspan=\"2\" class=\"tbl-head\">\r\n                KYC DETAILS\r\n            </th>\r\n        </thead>\r\n        <tbody *ngIf=\"kycSub\">\r\n                <tr>\r\n                        <th class=\"algnRyt\">\r\n                            Full Name\r\n                        </th>\r\n                        <td class=\"font16\">\r\n                                {{kycdetails?.FirstName}} {{kycdetails?.LastName}}\r\n                        </td>\r\n                    </tr>\r\n                    \r\n                    <tr>\r\n                            <th class=\"algnRyt\">\r\n                                Passport Number\r\n                            </th>\r\n                            <td class=\"font16\">\r\n                                    {{kycdetails?.PassportNumber}}\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                                <th class=\"algnRyt\">\r\n                                    Country\r\n                                </th>\r\n                                <td class=\"font16\">\r\n                                        {{kycdetails?.Country}}\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                    <th class=\"algnRyt\">\r\n                                     Date of Birth\r\n                                    </th>\r\n                                    <td class=\"font16\">\r\n                                            {{kycdetails?.Dob}}\r\n                                    </td>\r\n                                </tr>\r\n                                \r\n        </tbody>\r\n        \r\n    </table>\r\n   \r\n</div>\r\n\r\n\r\n<div *ngIf=\"cnindoc\">\r\n    <div class=\"table-responsive styling\" *ngIf=\"kycSubBlw\">\r\n        <table class=\"table \">\r\n            <thead>\r\n                <th colspan=\"2\" class=\"tbl-head\">\r\n                    ID DOCUMENTS\r\n                </th>\r\n            </thead>\r\n            \r\n                        <tr>\r\n                            <td class=\"cntrAlgn\">\r\n                                <img class=\"kycImg\" src=\"{{kycdetails?.CnicBack}}\">\r\n                            </td>\r\n                            <td class=\"cntrAlgn\">\r\n                                    <img class=\"kycImg\" src=\"{{kycdetails?.CnicFront}}\">\r\n                                </td>\r\n                        </tr>\r\n        </table>\r\n       \r\n    </div>\r\n</div>\r\n\r\n\r\n<div *ngIf=\"passdoc\">\r\n    <div class=\"table-responsive styling\" *ngIf=\"kycSubBlw1\">\r\n        <table class=\"table \">\r\n            <thead>\r\n                <th colspan=\"2\" class=\"tbl-head\">\r\n                    PASSPORT DOCUMENTS\r\n                </th>\r\n            </thead>\r\n            \r\n                        <tr>\r\n                            <td class=\"cntrAlgn\">\r\n                                <img class=\"kycImg\" src=\"{{kycdetails?.PassportFront}}\">\r\n                            </td>\r\n                            <td class=\"cntrAlgn\">\r\n                                    <img class=\"kycImg\" src=\"{{kycdetails?.PassportBack}}\">\r\n                                </td>\r\n                        </tr>\r\n        </table>\r\n       \r\n    </div>\r\n</div>\r\n\r\n<div class=\"table-responsive styling\" *ngIf=\"kycNotSub\">\r\n    <table class=\"table nn-tbl\">\r\n        <thead>\r\n            <th colspan=\"2\" class=\"tbl-head\">\r\n                KYC DETAILS\r\n            </th>\r\n        </thead>\r\n       \r\n        <div class=\"not-sub\" >\r\n            KYC details are not supplied by the user.\r\n        </div>                   \r\n     \r\n       \r\n    </table>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/theme/kyc-detail/kyc-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KycDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/services/smart.contract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utility_Toast__ = __webpack_require__("../../../../../src/app/theme/utility/Toast.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__web3_contracts_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/web3/contracts/smart.contract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_admin_service__ = __webpack_require__("../../../../../src/app/theme/services/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery__ = __webpack_require__("../../../../jquery/dist/jquery.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var KycDetailComponent = (function () {
    function KycDetailComponent(UserService, _adminService, router) {
        this.UserService = UserService;
        this._adminService = _adminService;
        this.router = router;
        this.userDetail = [];
        this.rowsFilter = [];
        this.tempFilter = [];
        this.kycSub = false;
        this.CnicDiv = false;
        this.PassDiv = false;
        this.kycNotSub = false;
        this.kycSubBlw = false;
        this.kycSubBlw1 = false;
        this.cnindoc = false;
        this.passdoc = false;
    }
    KycDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.toast = new __WEBPACK_IMPORTED_MODULE_3__utility_Toast__["a" /* Toast */]();
        this.userid = localStorage.getItem('UserIdAgainstEachUser');
        console.log("id against each user" + this.userid);
        this.userObject = JSON.parse(localStorage.getItem('userObject'));
        console.log(this.userObject);
        this.UserService.getKycDetails(this.userid).subscribe(function (response) {
            if (response.code = 200) {
                _this.kycSub = true;
                _this.kycNotSub = false;
                _this.kycSubBlw = true;
                _this.kycSubBlw1 = true;
                _this.kycdetails = response.data;
                if (_this.kycdetails.flag) {
                    _this.CnicDiv = true;
                    _this.PassDiv = false;
                    _this.cnindoc = true;
                    _this.passdoc = false;
                }
                else {
                    _this.PassDiv = true;
                    _this.CnicDiv = false;
                    _this.cnindoc = false;
                    _this.passdoc = true;
                }
                console.log(_this.kycdetails);
            }
        }, function (err) {
            console.log(JSON.parse(err._body));
            _this.kycNotSub = true;
            _this.kycSub = false;
            _this.kycSubBlw = false;
            _this.kycSubBlw1 = false;
            /*  var obj  = JSON.parse(err._body);
             console.log(obj);
             $("#snackbar").html(obj.message);
             this.toast.showToast(); */
        });
        console.log("in kyc ");
        this.viewingUser = JSON.parse(localStorage.getItem('viewingUser'));
    };
    KycDetailComponent.prototype.VerifyMethod = function () {
        var _this = this;
        this._adminService.accountVerification(this.userObject.token, this.userid, '2').subscribe(function (response) {
            __WEBPACK_IMPORTED_MODULE_6_jquery__('#verifyBtn').attr('disabled', 'disabled');
            __WEBPACK_IMPORTED_MODULE_6_jquery__('#rejectBtn').removeAttr('disabled', 'disabled');
            console.log(response);
            _this.tempFilter.forEach(function (element) {
                if (element._id == _this.userDetail._id) {
                    element.isKycVerified = 2;
                    localStorage.setItem('viewingUser', JSON.stringify(_this.viewingUser));
                    _this.viewingUser = JSON.parse(localStorage.getItem('viewingUser'));
                }
            }, function (err) {
                var obj = JSON.parse(err._body);
                if (obj.code == 404) {
                    localStorage.clear();
                    _this.router.navigateByUrl('/');
                }
            });
            _this.kycStatus = 'Accepted';
        });
    };
    KycDetailComponent.prototype.rejectMethod = function () {
        var _this = this;
        this._adminService.accountVerification(this.userObject.token, this.userid, '3').subscribe(function (response) {
            __WEBPACK_IMPORTED_MODULE_6_jquery__('#rejectBtn').attr('disabled', 'disabled');
            __WEBPACK_IMPORTED_MODULE_6_jquery__('#verifyBtn').removeAttr('disabled', 'disabled');
            console.log(_this.viewingUser);
            _this.tempFilter.forEach(function (element) {
                if (element._id == _this.userDetail._id) {
                    element.isKycVerified = 3;
                    localStorage.setItem('viewingUser', JSON.stringify(_this.viewingUser));
                    _this.viewingUser = JSON.parse(localStorage.getItem('viewingUser'));
                    console.log(_this.viewingUser);
                }
            });
            _this.kycStatus = 'Rejected';
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    KycDetailComponent.prototype.back = function () {
        this.router.navigateByUrl('/admin/users');
    };
    KycDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contract',
            template: __webpack_require__("../../../../../src/app/theme/kyc-detail/kyc-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/kyc-detail/kyc-detail.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_smart_contract_service__["a" /* SmartContractService */], __WEBPACK_IMPORTED_MODULE_4__web3_contracts_smart_contract_service__["a" /* ContractsService */], __WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_5__services_admin_service__["a" /* AdminService */]],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_5__services_admin_service__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */]])
    ], KycDetailComponent);
    return KycDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/kyc-detail/kyc-detail.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KycDetailModule", function() { return KycDetailModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_chartjs__ = __webpack_require__("../../../../angular2-chartjs/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_chartjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_chartjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__kyc_detail_component__ = __webpack_require__("../../../../../src/app/theme/kyc-detail/kyc-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__kyc_detail_routing_module__ = __webpack_require__("../../../../../src/app/theme/kyc-detail/kyc-detail-routing.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var KycDetailModule = (function () {
    function KycDetailModule() {
    }
    KycDetailModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_2__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_8__kyc_detail_routing_module__["a" /* KycDetailRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_3_angular2_chartjs__["ChartModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4_angular2_notifications__["SimpleNotificationsModule"].forRoot()
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_7__kyc_detail_component__["a" /* KycDetailComponent */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__kyc_detail_component__["a" /* KycDetailComponent */]]
        })
    ], KycDetailModule);
    return KycDetailModule;
}());



/***/ })

});
//# sourceMappingURL=kyc-detail.module.chunk.js.map