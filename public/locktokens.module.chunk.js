webpackJsonp(["locktokens.module"],{

/***/ "../../../../../src/app/theme/locktokens/locktokens.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"error-div\" class=\"row\" style=\"display: none\">\r\n  <div class=\"col-md-12 block\">\r\n    <p class=\"generic-error\">{{DAPPError}}</p>\r\n  </div>\r\n</div>\r\n<div id=\"working-div\">\r\n  <div id=\"userList\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n\r\n        <app-card [title]=\"'Users List'\" [cardOptionBlock]=\"true\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-3\">\r\n              <div class=\"form-group\">\r\n                <input type='text' class=\"form-control input-sm m-l-10\" [(ngModel)]=\"email\" placeholder='Search Eth Address' />\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"col-sm-3\">\r\n              <label class=\"dt-cust-search\">\r\n                <div class=\"form-group\">\r\n                  <button class=\"btn btn-primary\" (click)=\"searchUsers()\">Search</button>\r\n                </div>\r\n              </label>\r\n              <label class=\"dt-cust-search\">\r\n                <div class=\"form-group\">\r\n                  <button class=\"btn btn-primary\" style=\"background-color:red\" (click)=\"clearSearch()\">Clear</button>\r\n                </div>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <ngx-datatable #userListTable class='data-table' [loadingIndicator]=\"loadingIndicator\" [scrollbarH]=\"true\" [columns]=\"columns\"\r\n            [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"50\" [count]=\"pageSize\" [externalPaging]=\"true\"\r\n            [offset]=\"0\" [limit]=\"10\" [rows]='rowsFilter' (select)='onSelect($event)' (page)='setPage($event)'>\r\n            <ngx-datatable-column name=\"EthAddress\" sortable=\"false\" prop=\"EthAddress\" style=\"width:40%\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n\r\n            <ngx-datatable-column name=\"Locked Tokens\" sortable=\"false\" prop=\"LockedTokens\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Send Tokens\" sortable=\"false\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                <button *ngIf=\"row?.isLockedTokenSent == false\" class=\"view-btn btn btn-primary view-detail-btn\" style=\"width:80%;\" (click)=\"sendTokens(row?._id)\">\r\n                  <i class=\"fa fa-arrow\"> Send Tokens</i>\r\n                </button>\r\n                <button *ngIf=\"row?.isLockedTokenSent == true\" class=\"view-btn btn btn-primary view-detail-btn\" style=\"width:80%;\" (click)=\"viewTransaction(row?._id)\">\r\n                  <i class=\"fa fa-arrow\"> View Transaction</i>\r\n                </button>\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Token Sent Status\" sortable=\"false\" prop=\"isLockedTokenSent\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n          </ngx-datatable>\r\n        </app-card>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div id=\"detail\">\r\n    <div class=\"tableDiv\">\r\n      <button class=\"btn btn-primary go-back\" (click)=\"back()\">Go Back</button>\r\n      <div class=\"go-back\" style=\"float:right;\" id=\"btnDiv\">\r\n        <button type=\"button\" class=\"btn btn-outline-success\" id=\"verifyBtn\" (click)=\"VerifyMethod()\">Verify</button>\r\n        <button type=\"button\" class=\"btn btn-outline-danger\" id=\"rejectBtn\" (click)=\"rejectMethod()\">Reject</button>\r\n      </div>\r\n      <table class=\"table table-hover\" style=\"background:white;\">\r\n        <tbody>\r\n          <tr>\r\n            <th>Account Verification Status</th>\r\n            <td>{{kycStatus}}</td>\r\n          </tr>\r\n          <tr>\r\n            <th>Email</th>\r\n            <td>{{userDetail.Email}}</td>\r\n          </tr>\r\n          <tr>\r\n            <th>Nix Token</th>\r\n            <td>{{userDetail.Tokens}}</td>\r\n          </tr>\r\n\r\n          <tr>\r\n            <th>\r\n              Ethereum Address\r\n            </th>\r\n            <td>{{userDetail.EthAddress}}</td>\r\n          </tr>\r\n\r\n          <!-- <tr>\r\n          <th>BTG Fund Reciever Address</th>\r\n          <td>{{userDetail.BTGWalletRecieverAddress}}</td>\r\n        </tr>\r\n        <tr>\r\n          <th>DASH Fund Reciever Address</th>\r\n          <td>{{userDetail.DASHWalletRecieverAddress}}</td>\r\n        </tr>\r\n        <tr>\r\n          <th>NEO Fund Reciever Address</th>\r\n          <td>{{userDetail.NEOWalletRecieverAddress}}</td>\r\n        </tr> -->\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n\r\n\r\n    <table class=\"table table-hover\" align=\"center\" style=\"background:white;\">\r\n      <thead>\r\n        <th>Tx Hash</th>\r\n        <th>Amount Receive</th>\r\n        <th>Nix Token</th>\r\n        <th>Crypto Live Rate</th>\r\n        <th>Token Rate</th>\r\n        <th>Transaction Type</th>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let item of userDetail.allIncomingTransactions\">\r\n          <td style=\"width: 135px;display: block;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;margin: 0;\">\r\n            <a href=\"{{item.txDetailURL}}\" target=\"_blank\" style=\"color:#f48928\">{{item.txHash}}</a>\r\n          </td>\r\n          <td *ngIf=\"item.amountRecieve == 0\">N/A</td>\r\n          <td *ngIf=\"item.amountRecieve != 0\">{{item.amountRecieve}}</td>\r\n          <td *ngIf=\"item.totalCoins == 0\">N/A</td>\r\n          <td *ngIf=\"item.totalCoins != 0\">{{item.totalCoins}}</td>\r\n          <td *ngIf=\"item.cryptoLiveRate == 0\">N/A</td>\r\n          <td *ngIf=\"item.cryptoLiveRate != 0\">{{item.cryptoLiveRate}}</td>\r\n          <td *ngIf=\"item.rateForToken == 0\">N/A</td>\r\n          <td *ngIf=\"item.rateForToken != 0\">{{item.rateForToken}}</td>\r\n          <td>{{item.transactionType}}</td>\r\n        </tr>\r\n        <tr *ngIf=\"show\">\r\n          <td colspan=\"6\">No Transactions made by User\r\n          </td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n\r\n\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/theme/locktokens/locktokens.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#detail {\n  display: none;\n  width: 100%; }\n\n.go-back {\n  margin-bottom: 15px; }\n\n.datatable-body-cell-label {\n  margin-top: -10px !important; }\n\n.view-detail-btn {\n  font-size: 15px;\n  font-weight: 500;\n  width: 50%;\n  height: 44px;\n  margin-top: -10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/locktokens/locktokens.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LockTokensComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_admin_service__ = __webpack_require__("../../../../../src/app/theme/services/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/web3/contracts/smart.contract.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LockTokensComponent = (function () {
    function LockTokensComponent(_userService, router, _adminService, _web3ContractService) {
        var _this = this;
        this._userService = _userService;
        this.router = router;
        this._adminService = _adminService;
        this._web3ContractService = _web3ContractService;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.userDetail = [];
        this.pageNumber = 0;
        this.pageSize = 0;
        this.email = "";
        this._transactionBaseURL = "";
        this.show = false;
        this._web3ContractService.connect().then(function (result) {
        }).catch(function (err) {
            $('#working-div').hide();
            $('#error-div').show();
            _this.DAPPError = err;
        });
    }
    LockTokensComponent.prototype.ngOnInit = function () {
        this.userObject = JSON.parse(localStorage.getItem('userObject'));
        if (this.userObject == null || this.userObject === undefined) {
            this.router.navigateByUrl('/');
        }
        var self = this;
        if (document.getElementById) {
            window.alert = function (txt) {
                self.createCustomAlert(txt);
            };
        }
        this.getUserList();
        var contractAttributes = JSON.parse(localStorage.getItem("contract_attributes"));
        this._transactionBaseURL = contractAttributes.TxDetailUrl;
    };
    LockTokensComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        this.pageNumber = pageInfo.offset;
        this._userService.getPrivateSaleUsersList(this.userObject.token, this.pageNumber).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    LockTokensComponent.prototype.getUserList = function () {
        var _this = this;
        $('#userList').show();
        this._userService.getPrivateSaleUsersList(this.userObject.token, this.pageNumber).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    LockTokensComponent.prototype.searchUsers = function () {
        var _this = this;
        this._userService.getPrivateSaleUsersList(this.userObject.token, this.pageNumber, this.email).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    LockTokensComponent.prototype.sendTokens = function (id) {
        var _this = this;
        debugger;
        this._web3ContractService.getTokenLockTime()
            .then(function (lockTimeResult) {
            lockTimeResult = lockTimeResult * 1000;
            var date = new Date();
            var timestamp = date.getMilliseconds();
            if (timestamp > lockTimeResult) {
                _this.rowsFilter = _this.tempFilter;
                var userDetailTokensToSent = _this.rowsFilter.find(function (element) {
                    return element._id == id;
                });
                if (userDetailTokensToSent.isLockedTokenSent == false) {
                    var Total_Tokens = userDetailTokensToSent.LockedTokenSentHash;
                    var userEthAddress = userDetailTokensToSent.EthAddress;
                    if (Total_Tokens <= 0) {
                        alert("User has not purchased any tokens yet");
                        return;
                    }
                    if (userEthAddress === undefined) {
                        alert("Eth Address is not provided by the User");
                        return;
                    }
                    _this._web3ContractService;
                    _this._web3ContractService.getEthereumBalance()
                        .then(function (eth_balance) {
                        if (eth_balance <= 0) {
                            alert("You dont have enough ethers to transfer Tokens");
                            return;
                        }
                        _this._web3ContractService.getTokenBalance().then(function (balance) {
                            if (balance <= 0) {
                                alert("You dont have enough balance to transfer Tokens");
                                return;
                            }
                            if (balance < Total_Tokens) {
                                alert("Insufficient Token Balance. You have " + balance + " tokens");
                                return;
                            }
                            /// TODO
                            _this._web3ContractService.releasePrivateLockToken(userEthAddress).then(function (response) {
                                console.log(response);
                                if (response !== undefined) {
                                    var index = _this.rowsFilter.findIndex(function (element) {
                                        return element._id == id;
                                    });
                                    _this.rowsFilter[index].isLockedTokenSent = true;
                                    _this.rowsFilter[index].LockedTokenSentHash = response;
                                    _this._userService.updateTokenStateOfPrivateSaleUser(_this.userObject.token, response, userDetailTokensToSent.EthAddress).subscribe(function (service_response) {
                                        console.log(service_response);
                                    }, function (err) {
                                        var obj = JSON.parse(err._body);
                                        if (obj.code == 404) {
                                            localStorage.clear();
                                            _this.router.navigateByUrl('/');
                                        }
                                    });
                                }
                                else {
                                    alert("Transaction Failed");
                                }
                            }).catch(function (err) {
                                alert("Error from Metamask");
                            });
                        }).catch(function (err) {
                            alert("Error from Metamask");
                        });
                    })
                        .catch(function (err) {
                        alert("Error from Metamask");
                    });
                }
                else {
                    alert("Tokens have already been sent to the user");
                }
            }
            else {
                console.log(lockTimeResult);
                var date = new Date(lockTimeResult * 1000);
                alert("The tokens will be transferred after ICO completion " + date);
            }
        })
            .catch(function (error) {
            alert("Error from Smart Contract");
        });
    };
    LockTokensComponent.prototype.viewTransaction = function (id) {
        var userDetailTokensToSent = this.rowsFilter.find(function (element) {
            return element._id == id;
        });
        console.log(userDetailTokensToSent);
        window.open(this._transactionBaseURL + userDetailTokensToSent.TokenSentHash, '_blank');
    };
    LockTokensComponent.prototype.clearSearch = function () {
        var _this = this;
        this.email = "";
        this._userService.getPrivateSaleUsersList(this.userObject.token, 0).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    LockTokensComponent.prototype.createCustomAlert = function (txt) {
        var d = document;
        if (d.getElementById("modalContainer"))
            return;
        var mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";
        var alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if (d.all && !window)
            alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
        alertObj.style.visibility = "visible";
        var h1 = alertObj.appendChild(d.createElement("h1"));
        h1.appendChild(d.createTextNode("Ooops!"));
        var msg = alertObj.appendChild(d.createElement("p"));
        msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;
        var btn = alertObj.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode("Ok"));
        btn.href = "#";
        btn.focus();
        var self = this;
        btn.onclick = function () { self.removeCustomAlert(); return false; };
        alertObj.style.display = "block";
    };
    LockTokensComponent.prototype.removeCustomAlert = function () {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    };
    LockTokensComponent.prototype.ful = function () {
        alert('Alert this pages');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], LockTokensComponent.prototype, "userListTable", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], LockTokensComponent.prototype, "userDetailTable", void 0);
    LockTokensComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-locktokens',
            template: __webpack_require__("../../../../../src/app/theme/locktokens/locktokens.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/locktokens/locktokens.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__services_admin_service__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__["a" /* ContractsService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_4__services_admin_service__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__["a" /* ContractsService */]])
    ], LockTokensComponent);
    return LockTokensComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/locktokens/locktokens.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LockTokensModule", function() { return LockTokensModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__locktokens_routing_module__ = __webpack_require__("../../../../../src/app/theme/locktokens/locktokens.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__locktokens_component__ = __webpack_require__("../../../../../src/app/theme/locktokens/locktokens.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var LockTokensModule = (function () {
    function LockTokensModule() {
    }
    LockTokensModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__locktokens_routing_module__["a" /* LockTokensRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__locktokens_component__["a" /* LockTokensComponent */]]
        })
    ], LockTokensModule);
    return LockTokensModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/locktokens/locktokens.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LockTokensRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__locktokens_component__ = __webpack_require__("../../../../../src/app/theme/locktokens/locktokens.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__locktokens_component__["a" /* LockTokensComponent */],
        data: {
            title: 'Users',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var LockTokensRoutingModule = (function () {
    function LockTokensRoutingModule() {
    }
    LockTokensRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], LockTokensRoutingModule);
    return LockTokensRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=locktokens.module.chunk.js.map