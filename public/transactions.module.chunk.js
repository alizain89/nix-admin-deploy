webpackJsonp(["transactions.module"],{

/***/ "../../../../../src/app/theme/transactions/transactions.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12\">\r\n\r\n    <app-card [title]=\"'Transactions'\" [cardOptionBlock]=\"true\">\r\n      <div class=\"row\">\r\n        <!-- <div class=\"col-sm-3\">\r\n          <div class=\"form-group\">\r\n            <label>Search Currency Type: </label>\r\n            <select class=\"form-control input-sm m-l-10\" (change)=\"searchWithType($event)\">\r\n              <option value=\"-1\">All</option>\r\n              <option value=\"BCH\">BCH</option>\r\n              <option value=\"LTC\">LTC</option>\r\n              <option value=\"BTC\">BTC</option>\r\n              <option value=\"ETH\">ETH</option>\r\n              <!-- <option value=\"NEO\">NEO</option>\r\n              <option value=\"DASH\">DASH</option>\r\n              <option value=\"BTG\">BTG</option> \r\n            </select>\r\n          </div>\r\n        </div> -->\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"form-group\">\r\n            <label>Search Hash: </label>\r\n            <input type='text' class=\"form-control input-sm m-l-10\" [(ngModel)]=\"txHash\" placeholder='Search Hash' />\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"form-group\">\r\n            <label>Email: </label>\r\n            <input type='text' class=\"form-control input-sm m-l-10\" [(ngModel)]=\"email\" placeholder='Search Email' />\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-3\" style=\"padding-top: 22px;\">\r\n          <div class=\"form-group\">\r\n            <button class=\"btn btn-primary\" (click)=\"searchTransactions()\">Search</button>\r\n            <button class=\"btn btn-primary\" style=\"background-color:red\" (click)=\"clearSearch()\">Clear</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <ngx-datatable #TransactionListTable class='data-table' [scrollbarH]=\"true\" [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\"\r\n        [footerHeight]=\"50\" [rowHeight]=\"50\" [limit]=\"10\" [count]=\"pageSize\" [externalPaging]=\"true\" [offset]=\"0\" [rows]='rowsFilter'\r\n        (select)='onSelect($event)' (page)='setPage($event)'>\r\n        <ngx-datatable-column name=\"Tx Hash\" sortable=\"false\" prop=\"txHash\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            <a style=\"cursor: pointer;color:#5ebb44\" (click)=\"viewDetail(value)\" target=\"_blank\">{{value}}</a>\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"Email\" sortable=\"false\" prop=\"user_id.Email\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            {{value}}\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"Amount Received\" sortable=\"false\" prop=\"amountRecieve\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            {{value}}\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"NiX Token\" sortable=\"false\" prop=\"totalCoins\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            {{value}}\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"Crypto Live Rate\" sortable=\"false\" prop=\"cryptoLiveRate\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            {{value}}\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n        <ngx-datatable-column name=\"TransactionType\" sortable=\"false\" prop=\"transactionType\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            {{value}}\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n      </ngx-datatable>\r\n    </app-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/theme/transactions/transactions.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/transactions/transactions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TransactionsComponent = (function () {
    function TransactionsComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.TypeValue = 0;
        this.pageNumber = 0;
        this.pageSize = 0;
        this.email = null;
        this.txHash = null;
    }
    TransactionsComponent.prototype.ngOnInit = function () {
        this.user = JSON.parse(localStorage.getItem("user"));
        if (this.user == null || this.user === undefined) {
        }
        this.getTransactionsList();
    };
    TransactionsComponent.prototype.getTransactionsList = function () {
        var _this = this;
        console.log(this.user.token);
        this._userService.transactionsList(this.user.token, this.pageNumber, 10, null, null).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allTransactions;
            _this.tempFilter = res.allTransactions;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    TransactionsComponent.prototype.clearSearch = function () {
        this.email = "";
        this.txHash = "";
        this.getTransactionsList();
    };
    TransactionsComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        this.pageNumber = pageInfo.offset;
        this._userService.transactionsList(this.user.token, this.pageNumber, 10, this.email, this.txHash).subscribe(function (res) {
            _this.rowsFilter = res.allTransactions;
            _this.tempFilter = res.allTransactions;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    TransactionsComponent.prototype.viewDetail = function (txHash) {
        var obj = this.rowsFilter.find(function (x) { return x.txHash == txHash; });
        var win = window.open(obj.txDetailURL, '_blank');
        win.focus();
    };
    TransactionsComponent.prototype.searchTransactions = function () {
        var _this = this;
        if (this.email === undefined || this.email == null || this.email == "") {
            this.email = null;
        }
        if (this.txHash === undefined || this.txHash == null || this.txHash == "") {
            this.txHash = null;
        }
        this._userService.transactionsList(this.user.token, 0, 10, this.email, this.txHash).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allTransactions;
            _this.tempFilter = res.allTransactions;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["DatatableComponent"])
    ], TransactionsComponent.prototype, "TransactionListTable", void 0);
    TransactionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-transactions',
            template: __webpack_require__("../../../../../src/app/theme/transactions/transactions.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/transactions/transactions.component.scss"), __webpack_require__("../../../../../src/assets/icon/icofont/css/icofont.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */]])
    ], TransactionsComponent);
    return TransactionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/transactions/transactions.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsModule", function() { return TransactionsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__transactions_component__ = __webpack_require__("../../../../../src/app/theme/transactions/transactions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__transactions_routing_module__ = __webpack_require__("../../../../../src/app/theme/transactions/transactions.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var TransactionsModule = (function () {
    function TransactionsModule() {
    }
    TransactionsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__transactions_routing_module__["a" /* TransactionsRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_6__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_0__transactions_component__["a" /* TransactionsComponent */]]
        })
    ], TransactionsModule);
    return TransactionsModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/transactions/transactions.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionsRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__transactions_component__ = __webpack_require__("../../../../../src/app/theme/transactions/transactions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__transactions_component__["a" /* TransactionsComponent */],
        data: {
            title: 'Transactions',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var TransactionsRoutingModule = (function () {
    function TransactionsRoutingModule() {
    }
    TransactionsRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], TransactionsRoutingModule);
    return TransactionsRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=transactions.module.chunk.js.map