webpackJsonp(["unsolvedtransactions.module"],{

/***/ "../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12\">\r\n\r\n    <app-card [title]=\"'Transactions'\" [cardOptionBlock]=\"true\">\r\n      <div class=\"row\">\r\n        <!-- <div class=\"col-sm-3\">\r\n          <div class=\"form-group\">\r\n            <label>Search Currency Type: </label>\r\n            <select class=\"form-control input-sm m-l-10\" (change)=\"searchWithType($event)\">\r\n              <option value=\"-1\">All</option>\r\n              <option value=\"BCH\">BCH</option>\r\n              <option value=\"LTC\">LTC</option>\r\n              <option value=\"BTC\">BTC</option>\r\n              <option value=\"ETH\">ETH</option>\r\n              <!-- <option value=\"NEO\">NEO</option>\r\n              <option value=\"DASH\">DASH</option>\r\n              <option value=\"BTG\">BTG</option> \r\n            </select>\r\n          </div>\r\n        </div> -->\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"form-group\">\r\n            <label>Search Hash: </label>\r\n            <input type='text' class=\"form-control input-sm m-l-10\" [(ngModel)]=\"txHash\" placeholder='Search Hash' />\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-3\" style=\"padding-top: 22px;\">\r\n          <div class=\"form-group\">\r\n            <button class=\"btn btn-primary\" (click)=\"searchTransactions()\">Search</button>\r\n            <button class=\"btn btn-primary\" style=\"background-color:red\" (click)=\"clearSearch()\">Clear</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <ngx-datatable #TransactionListTable class='data-table' [scrollbarH]=\"true\" [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\"\r\n        [footerHeight]=\"50\" [rowHeight]=\"50\" [limit]=\"10\" [count]=\"pageSize\" [externalPaging]=\"true\" [offset]=\"0\" [rows]='rowsFilter'\r\n        (select)='onSelect($event)'>\r\n        <ngx-datatable-column name=\"Tx Hash\" sortable=\"false\" prop=\"txHash\">\r\n          <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n            <a style=\"cursor: pointer;color:#5ebb44\" (click)=\"viewDetail(value)\" target=\"_blank\">{{value}}</a>\r\n          </ng-template>\r\n        </ngx-datatable-column>\r\n\r\n      </ngx-datatable>\r\n    </app-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnsolvedTransactionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UnsolvedTransactionsComponent = (function () {
    function UnsolvedTransactionsComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.TypeValue = 0;
        this.pageNumber = 0;
        this.pageSize = 0;
        this.txHash = null;
    }
    UnsolvedTransactionsComponent.prototype.ngOnInit = function () {
        this.userObject = JSON.parse(localStorage.getItem("userObject"));
        this.getTransactionsList();
        var contractAttributes = JSON.parse(localStorage.getItem("contract_attributes"));
        this._transactionBaseURL = contractAttributes.TxDetailUrl;
    };
    UnsolvedTransactionsComponent.prototype.getTransactionsList = function () {
        var _this = this;
        this._userService.getUnresolvedTransactionsList(this.userObject.token, this.pageNumber, 10, null).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.txs;
            _this.tempFilter = res.txs;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UnsolvedTransactionsComponent.prototype.clearSearch = function () {
        this.txHash = "";
        this.getTransactionsList();
    };
    UnsolvedTransactionsComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        this.pageNumber = pageInfo.offset;
        this._userService.getUnresolvedTransactionsList(this.userObject.token, this.pageNumber, 10, this.txHash).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allTransactions;
            _this.tempFilter = res.allTransactions;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UnsolvedTransactionsComponent.prototype.viewDetail = function (txHash) {
        var obj = this.rowsFilter.find(function (x) { return x.txHash == txHash; });
        var win = window.open(this._transactionBaseURL + txHash, '_blank');
        win.focus();
    };
    UnsolvedTransactionsComponent.prototype.searchTransactions = function () {
        var _this = this;
        if (this.txHash === undefined || this.txHash == null || this.txHash == "") {
            this.txHash = null;
        }
        this._userService.getUnresolvedTransactionsList(this.userObject.token, this.pageNumber, 10, this.txHash).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allTransactions;
            _this.tempFilter = res.allTransactions;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["DatatableComponent"])
    ], UnsolvedTransactionsComponent.prototype, "TransactionListTable", void 0);
    UnsolvedTransactionsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-unsolved-transactions',
            template: __webpack_require__("../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.scss"), __webpack_require__("../../../../../src/assets/icon/icofont/css/icofont.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */]])
    ], UnsolvedTransactionsComponent);
    return UnsolvedTransactionsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnsolvedTransactionsModule", function() { return UnsolvedTransactionsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__unsolvedtransactions_component__ = __webpack_require__("../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__unsolvedtransactions_routing_module__ = __webpack_require__("../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var UnsolvedTransactionsModule = (function () {
    function UnsolvedTransactionsModule() {
    }
    UnsolvedTransactionsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__unsolvedtransactions_routing_module__["a" /* UnsolvedTransactionsRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_0__unsolvedtransactions_component__["a" /* UnsolvedTransactionsComponent */]]
        })
    ], UnsolvedTransactionsModule);
    return UnsolvedTransactionsModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UnsolvedTransactionsRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__unsolvedtransactions_component__ = __webpack_require__("../../../../../src/app/theme/unsolvedtransactions/unsolvedtransactions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__unsolvedtransactions_component__["a" /* UnsolvedTransactionsComponent */],
        data: {
            title: 'Transactions',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var UnsolvedTransactionsRoutingModule = (function () {
    function UnsolvedTransactionsRoutingModule() {
    }
    UnsolvedTransactionsRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], UnsolvedTransactionsRoutingModule);
    return UnsolvedTransactionsRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=unsolvedtransactions.module.chunk.js.map