webpackJsonp(["users.module"],{

/***/ "../../../../../src/app/theme/users/users.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"error-div\" class=\"row\" style=\"display: none\">\r\n  <div class=\"col-md-12 block\">\r\n    <p class=\"generic-error\">{{DAPPError}}</p>\r\n  </div>\r\n</div>\r\n<div id=\"working-div\">\r\n  <div id=\"userList\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-12\">\r\n\r\n        <app-card [title]=\"'Users List'\" [cardOptionBlock]=\"true\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-3\">\r\n              <div class=\"form-group\">\r\n                <input type='text' class=\"form-control input-sm m-l-10\" [(ngModel)]=\"email\" placeholder='Search Email' />\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"col-sm-3\">\r\n              <label class=\"dt-cust-search\">\r\n                <div class=\"form-group\">\r\n                  <button class=\"btn btn-primary\" (click)=\"searchUsers()\">Search</button>\r\n                </div>\r\n              </label>\r\n              <label class=\"dt-cust-search\">\r\n                <div class=\"form-group\">\r\n                  <button class=\"btn btn-primary\" style=\"background-color:red\" (click)=\"clearSearch()\">Clear</button>\r\n                </div>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <ngx-datatable #userListTable class='data-table' [loadingIndicator]=\"loadingIndicator\" [scrollbarH]=\"true\" [columns]=\"columns\"\r\n            [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\" [rowHeight]=\"50\" [count]=\"pageSize\" [externalPaging]=\"true\"\r\n            [offset]=\"0\" [limit]=\"10\" [rows]='rowsFilter' (select)='onSelect($event)' (page)='setPage($event)'>\r\n            <ngx-datatable-column name=\"Email\" sortable=\"false\" prop=\"Email\" style=\"width:40%\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n\r\n            <ngx-datatable-column name=\"Tokens\" sortable=\"false\" prop=\"Tokens\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"View Detail\" sortable=\"false\" prop=\"_id\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                <button class=\"view-btn btn btn-primary view-detail-btn\" style=\"width:80%;\" (click)=\"view(value)\">\r\n                  <i class=\"fa fa-eye\"> View Detail</i>\r\n                </button>\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Send Tokens\" sortable=\"false\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                <button *ngIf=\"row?.isTokenSent == false\" class=\"view-btn btn btn-primary view-detail-btn\" style=\"width:80%;\" (click)=\"sendTokens(row?._id)\">\r\n                  <i class=\"fa fa-arrow\"> Send Tokens</i>\r\n                </button>\r\n                <button *ngIf=\"row?.isTokenSent == true\" class=\"view-btn btn btn-primary view-detail-btn\" style=\"width:80%;\" (click)=\"viewTransaction(row?._id)\">\r\n                  <i class=\"fa fa-arrow\"> View Transaction</i>\r\n                </button>\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"Token Sent Status\" sortable=\"false\" prop=\"isTokenSent\">\r\n              <ng-template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                {{value}}\r\n              </ng-template>\r\n            </ngx-datatable-column>\r\n          </ngx-datatable>\r\n        </app-card>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div id=\"detail\">\r\n    <div class=\"tableDiv\">\r\n      <button class=\"btn btn-primary go-back\" (click)=\"back()\">Go Back</button>\r\n      <button class=\"btn btn-primary go-back\" (click)=\"sendEmailVerificationLink()\">Send Email Verification Link</button>\r\n      <div class=\"go-back\" style=\"float:right;\" id=\"btnDiv\">\r\n          <button type=\"button\" class=\"btn btn-outline-success kycBtn\" id=\"KycBtn\" (click)=\"moveToKyc()\">KYC Detail</button>\r\n        <button type=\"button\" *ngIf=\"userDetail.isKycVerified == 1 || userDetail.isKycVerified == 3  \" class=\"btn btn-outline-success\" id=\"verifyBtn\" (click)=\"VerifyMethod()\">Verify</button>\r\n        <button type=\"button\" *ngIf=\"userDetail.isKycVerified == 1 || userDetail.isKycVerified ==2\" class=\"btn btn-outline-danger\" id=\"rejectBtn\" (click)=\"rejectMethod()\">Reject</button>\r\n      </div>\r\n      <table class=\"table table-hover\" style=\"background:white;\">\r\n        <tbody>\r\n          <tr>\r\n            <th>First Name</th>\r\n            <td>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userDetail.FirstName\" />\r\n              <div class=\"messages text-danger\" id=\"first_name_error\">First Name is Required</div>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <th>Last Name</th>\r\n            <td>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userDetail.LastName\" />\r\n              <div class=\"messages text-danger\" id=\"last_name_error\">Last Name is Required</div>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <th>\r\n              Ethereum Address\r\n            </th>\r\n            <td>\r\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"userDetail.EthAddress\" />\r\n              <div class=\"messages text-danger\" id=\"eth_address_error\">EthAddress is Required</div>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <th>\r\n              Country\r\n            </th>\r\n            <td>\r\n              <select id=\"country-dd\" class=\"form-control\" [(ngModel)]=\"userDetail.Country\">\r\n                <option *ngFor=\"let Country of countries\">\r\n                  {{Country.country}}\r\n                </option>\r\n              </select>\r\n              <div class=\"messages text-danger\" id=\"country_error\">Country is Required</div>\r\n            </td>\r\n          </tr>\r\n          <tr>\r\n            <td colspan=\"2\" style=\"padding-left: 30%\"><button class=\"btn btn-primary\" (click)=\"updateUser()\" style=\"width: 50%;\">Update User</button></td>\r\n          </tr>\r\n          <tr>\r\n            <th>Email</th>\r\n            <td>{{userDetail.Email}}</td>\r\n          </tr>\r\n          <tr>\r\n            <th>Account Verification Status</th>\r\n            <td>{{kycStatus}}   <button type=\"button\" class=\"btn btn-outline-success kycBtn kycbtnInTbl\" (click)=\"moveToKyc()\" >KYC Detail</button></td>\r\n          </tr>\r\n          <tr>\r\n            <th>NiX Token</th>\r\n            <td>{{userDetail.Tokens}}</td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n\r\n\r\n    <table class=\"table table-hover\" align=\"center\" style=\"background:white;\">\r\n      <thead>\r\n        <th>Tx Hash</th>\r\n        <th>Amount Receive</th>\r\n        <th>NiX Token</th>\r\n        <th>Crypto Live Rate</th>\r\n        <th>Token Rate</th>\r\n        <th>Transaction Type</th>\r\n      </thead>\r\n      <tbody>\r\n        <tr *ngFor=\"let item of userDetail.allIncomingTransactions\">\r\n          <td style=\"width: 135px;display: block;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;margin: 0;\">\r\n            <a href=\"{{item.txDetailURL}}\" target=\"_blank\" style=\"color:#f48928\">{{item.txHash}}</a>\r\n          </td>\r\n          <td *ngIf=\"item.amountRecieve == 0\">N/A</td>\r\n          <td *ngIf=\"item.amountRecieve != 0\">{{item.amountRecieve}}</td>\r\n          <td *ngIf=\"item.totalCoins == 0\">N/A</td>\r\n          <td *ngIf=\"item.totalCoins != 0\">{{item.totalCoins}}</td>\r\n          <td *ngIf=\"item.cryptoLiveRate == 0\">N/A</td>\r\n          <td *ngIf=\"item.cryptoLiveRate != 0\">{{item.cryptoLiveRate}}</td>\r\n          <td *ngIf=\"item.rateForToken == 0\">N/A</td>\r\n          <td *ngIf=\"item.rateForToken != 0\">{{item.rateForToken}}</td>\r\n          <td>{{item.transactionType}}</td>\r\n        </tr>\r\n        <tr *ngIf=\"show\">\r\n          <td colspan=\"6\">No Transactions made by User\r\n          </td>\r\n        </tr>\r\n      </tbody>\r\n    </table>\r\n  </div>\r\n</div>\r\n<div id=\"snackbar\"></div>"

/***/ }),

/***/ "../../../../../src/app/theme/users/users.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#detail {\n  display: none;\n  width: 100%; }\n\n.go-back {\n  margin-bottom: 15px; }\n\n.datatable-body-cell-label {\n  margin-top: -10px !important; }\n\n.view-detail-btn {\n  font-size: 15px;\n  font-weight: 500;\n  width: 50%;\n  height: 44px;\n  margin-top: -10px; }\n\n.text-danger {\n  display: none; }\n\n.kycBtn {\n  border: 1px solid #f48928 !important;\n  color: #f48928 !important; }\n\n.kycBtn:hover, .kycBtn:focus, .kycBtn:active {\n  background-color: #f48928 !important;\n  color: white !important; }\n\n.kycbtnInTbl {\n  padding: 5px 7px;\n  margin-left: 20px; }\n\n.kycBtn:focus, .kycBtn:active {\n  -webkit-box-shadow: unset !important;\n          box-shadow: unset !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/theme/users/users.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_user_service__ = __webpack_require__("../../../../../src/app/theme/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_admin_service__ = __webpack_require__("../../../../../src/app/theme/services/admin.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__ = __webpack_require__("../../../../../src/app/theme/web3/contracts/smart.contract.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__utility_Toast__ = __webpack_require__("../../../../../src/app/theme/utility/Toast.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var UsersComponent = (function () {
    function UsersComponent(_userService, router, _adminService, _web3ContractService, http) {
        var _this = this;
        this._userService = _userService;
        this.router = router;
        this._adminService = _adminService;
        this._web3ContractService = _web3ContractService;
        this.http = http;
        this.rowsFilter = [];
        this.tempFilter = [];
        this.userDetail = [];
        this.countries = [];
        this.pageNumber = 0;
        this.pageSize = 0;
        this.email = "";
        this._transactionBaseURL = "";
        this.show = false;
        this.toast = new __WEBPACK_IMPORTED_MODULE_7__utility_Toast__["a" /* Toast */]();
        this._web3ContractService.connect().then(function (result) {
        }).catch(function (err) {
            $('#working-div').hide();
            $('#error-div').show();
            _this.DAPPError = err;
        });
    }
    UsersComponent.prototype.getCountries = function () {
        var _this = this;
        return this.http.get('./assets/files/countries.json')
            .subscribe(function (res) {
            _this.countries = res.json();
        });
    };
    UsersComponent.prototype.ngOnInit = function () {
        this.getCountries();
        this.userObject = JSON.parse(localStorage.getItem('userObject'));
        if (this.userObject == null || this.userObject === undefined) {
            this.router.navigateByUrl('/');
        }
        var self = this;
        if (document.getElementById) {
            window.alert = function (txt) {
                self.createCustomAlert(txt);
            };
        }
        this.getUserList();
        var contractAttributes = JSON.parse(localStorage.getItem("contract_attributes"));
        this._transactionBaseURL = contractAttributes.TxDetailUrl;
    };
    UsersComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        this.pageNumber = pageInfo.offset;
        this._userService.getAllUserList(this.userObject.token, this.pageNumber).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UsersComponent.prototype.getUserList = function () {
        var _this = this;
        $('#userList').show();
        this._userService.getAllUserList(this.userObject.token, this.pageNumber).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UsersComponent.prototype.searchUsers = function () {
        var _this = this;
        this._userService.getAllUserList(this.userObject.token, this.pageNumber, this.email).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UsersComponent.prototype.moveToKyc = function () {
        this.router.navigateByUrl('/admin/kyc-detail');
    };
    UsersComponent.prototype.view = function (id) {
        var _this = this;
        var UserIdAgainstEachUser = id;
        this.show = false;
        this.rowsFilter = this.tempFilter;
        this.userDetail = this.rowsFilter.find(function (element) {
            return element._id == id;
        });
        localStorage.setItem("viewingUser", JSON.stringify(this.userDetail));
        localStorage.setItem('UserIdAgainstEachUser', UserIdAgainstEachUser);
        this._userService.getTransactionListOfUsers(this.userObject.token, this.userDetail._id).subscribe(function (response) {
            _this.userDetail.allIncomingTransactions = response.data;
            if (_this.userDetail.allIncomingTransactions.length == 0) {
                _this.show = true;
            }
            if (_this.userDetail.isKycVerified == 0) {
                _this.kycStatus = "Not Submitted";
            }
            else {
                if (_this.userDetail.isKycVerified == 1) {
                    _this.kycStatus = "Submitted";
                }
                else if (_this.userDetail.isKycVerified == 2) {
                    _this.kycStatus = "Accepted";
                    $('#verifyBtn').attr('disabled', 'disabled');
                }
                else if (_this.userDetail.isKycVerified == 3) {
                    _this.kycStatus = "Rejected";
                    $('#rejectBtn').attr('disabled', 'disabled');
                }
            }
            $('#country-dd').val(_this.userDetail.Country);
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
        $('#userList').hide();
        $('#detail').show();
    };
    UsersComponent.prototype.updateUser = function () {
        var _this = this;
        $('.text-danger').hide();
        if (!this.userDetail.FirstName) {
            $('#first_name_error').show();
            return;
        }
        if (!this.userDetail.LastName) {
            $('#last_name_error').show();
            return;
        }
        if (!this.userDetail.EthAddress) {
            $('#eth_address_error').show();
            return;
        }
        if (!this.userDetail.Country) {
            $('#country_error').show();
            return;
        }
        this._userService.updateUser(this.userDetail.FirstName, this.userDetail.LastName, this.userDetail.EthAddress, this.userDetail.Country, this.userDetail.Email, this.userObject.token).subscribe(function (response) {
            $("#snackbar").html(response.message);
            _this.toast.showToast();
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
            else {
                $("#snackbar").html(obj.message);
                _this.toast.showToast();
            }
        });
    };
    UsersComponent.prototype.sendEmailVerificationLink = function () {
        var _this = this;
        this._userService.sendEmailVerificationLink(this.userDetail._id, this.userObject.token).subscribe(function (response) {
            $("#snackbar").html(response.message);
            _this.toast.showToast();
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
            else {
                $("#snackbar").html(obj.message);
                _this.toast.showToast();
            }
        });
    };
    UsersComponent.prototype.sendTokens = function (id) {
        var _this = this;
        this.rowsFilter = this.tempFilter;
        var userDetailTokensToSent = this.rowsFilter.find(function (element) {
            return element._id == id;
        });
        if (userDetailTokensToSent.isTokenSent == false) {
            var Total_Tokens = userDetailTokensToSent.Tokens;
            var userEthAddress = userDetailTokensToSent.EthAddress;
            if (userDetailTokensToSent.isKycVerified != 2) {
                alert("User KYC is not Verified/Submitted");
                return;
            }
            if (Total_Tokens <= 0) {
                alert("User has not purchased any tokens yet");
                return;
            }
            if (userEthAddress === undefined) {
                alert("Eth Address is not provided by the User");
                return;
            }
            this._web3ContractService.getEthereumBalance()
                .then(function (eth_balance) {
                if (eth_balance <= 0) {
                    alert("You dont have enough ethers to transfer Tokens");
                    return;
                }
                _this._web3ContractService.getTokenBalance().then(function (balance) {
                    if (balance <= 0) {
                        alert("You dont have enough balance to transfer Tokens");
                        return;
                    }
                    if (balance < Total_Tokens) {
                        alert("Insufficient Token Balance. You have " + balance + " tokens");
                        return;
                    }
                    var tokens = parseFloat(Total_Tokens) * Math.pow(10, _this._web3ContractService.getDecimals());
                    _this._web3ContractService.transferTokens(userEthAddress, tokens).then(function (response) {
                        var index = _this.rowsFilter.findIndex(function (element) {
                            return element._id == id;
                        });
                        _this.rowsFilter[index].isTokenSent = true;
                        _this.rowsFilter[index].TokenSentHash = response;
                        _this._userService.updateTokenStateOfUser(_this.userObject.token, response, userDetailTokensToSent.Email).subscribe(function (service_response) {
                        }, function (err) {
                            var obj = JSON.parse(err._body);
                            if (obj.code == 404) {
                                localStorage.clear();
                                _this.router.navigateByUrl('/');
                            }
                        });
                    }).catch(function (err) {
                        alert("Error from Metamask");
                    });
                }).catch(function (err) {
                    alert("Error from Metamask");
                });
            })
                .catch(function (err) {
                alert("Error from Metamask");
            });
        }
        else {
            alert("Tokens have already been sent to the user");
        }
    };
    UsersComponent.prototype.viewTransaction = function (id) {
        var userDetailTokensToSent = this.rowsFilter.find(function (element) {
            return element._id == id;
        });
        window.open(this._transactionBaseURL + userDetailTokensToSent.TokenSentHash, '_blank');
    };
    UsersComponent.prototype.clearSearch = function () {
        var _this = this;
        this.email = "";
        this._userService.getAllUserList(this.userObject.token, 0).subscribe(function (res) {
            _this.pageSize = res.count;
            _this.rowsFilter = res.allUsers;
            _this.tempFilter = res.allUsers;
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    UsersComponent.prototype.back = function () {
        $('#userList').show();
        $('#detail').hide();
        $('.datatable-body').css('width', '100%');
    };
    UsersComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        // filter our data
        var temp = this.tempFilter.filter(function (d) {
            return d.Email.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rowsFilter = temp;
        // Whenever the filter changes, always go back to the first page
        this.userListTable.offset = 0;
    };
    UsersComponent.prototype.createCustomAlert = function (txt) {
        var d = document;
        if (d.getElementById("modalContainer"))
            return;
        var mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";
        var alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if (d.all && !window)
            alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
        alertObj.style.visibility = "visible";
        var h1 = alertObj.appendChild(d.createElement("h1"));
        h1.appendChild(d.createTextNode("Ooops!"));
        var msg = alertObj.appendChild(d.createElement("p"));
        msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;
        var btn = alertObj.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode("Ok"));
        btn.href = "#";
        btn.focus();
        var self = this;
        btn.onclick = function () { self.removeCustomAlert(); return false; };
        alertObj.style.display = "block";
    };
    UsersComponent.prototype.removeCustomAlert = function () {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    };
    UsersComponent.prototype.ful = function () {
        alert('Alert this pages');
    };
    UsersComponent.prototype.VerifyMethod = function () {
        var _this = this;
        this._adminService.accountVerification(this.userObject.token, this.userDetail._id, '2').subscribe(function (response) {
            $('#verifyBtn').attr('disabled', 'disabled');
            $('#rejectBtn').removeAttr('disabled', 'disabled');
            _this.tempFilter.forEach(function (element) {
                if (element._id == _this.userDetail._id) {
                    element.isKycVerified = 2;
                    localStorage.setItem("viewingUser", JSON.stringify(_this.userDetail));
                }
            }, function (err) {
                var obj = JSON.parse(err._body);
                if (obj.code == 404) {
                    localStorage.clear();
                    _this.router.navigateByUrl('/');
                }
            });
            _this.kycStatus = 'Accepted';
        });
    };
    UsersComponent.prototype.rejectMethod = function () {
        var _this = this;
        this._adminService.accountVerification(this.userObject.token, this.userDetail._id, '3').subscribe(function (response) {
            $('#rejectBtn').attr('disabled', 'disabled');
            $('#verifyBtn').removeAttr('disabled', 'disabled');
            _this.tempFilter.forEach(function (element) {
                if (element._id == _this.userDetail._id) {
                    element.isKycVerified = 3;
                    console.log(_this.userDetail);
                    localStorage.setItem("viewingUser", JSON.stringify(_this.userDetail));
                }
            });
            _this.kycStatus = 'Rejected';
        }, function (err) {
            var obj = JSON.parse(err._body);
            if (obj.code == 404) {
                localStorage.clear();
                _this.router.navigateByUrl('/');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], UsersComponent.prototype, "userListTable", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__swimlane_ngx_datatable__["DatatableComponent"])
    ], UsersComponent.prototype, "userDetailTable", void 0);
    UsersComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-users',
            template: __webpack_require__("../../../../../src/app/theme/users/users.component.html"),
            styles: [__webpack_require__("../../../../../src/app/theme/users/users.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_4__services_admin_service__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__["a" /* ContractsService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_user_service__["a" /* UserService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_4__services_admin_service__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_5__web3_contracts_smart_contract_service__["a" /* ContractsService */], __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Http */]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "../../../../../src/app/theme/users/users.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__users_routing_module__ = __webpack_require__("../../../../../src/app/theme/users/users.routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__users_component__ = __webpack_require__("../../../../../src/app/theme/users/users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var UsersModule = (function () {
    function UsersModule() {
    }
    UsersModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__users_routing_module__["a" /* UsersRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_0__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__users_component__["a" /* UsersComponent */]]
        })
    ], UsersModule);
    return UsersModule;
}());



/***/ }),

/***/ "../../../../../src/app/theme/users/users.routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsersRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__users_component__ = __webpack_require__("../../../../../src/app/theme/users/users.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__users_component__["a" /* UsersComponent */],
        data: {
            title: 'Users',
            icon: 'icon-layout-sidebar-left',
            caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
            status: false
        }
    }
];
var UsersRoutingModule = (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */]]
        })
    ], UsersRoutingModule);
    return UsersRoutingModule;
}());



/***/ })

});
//# sourceMappingURL=users.module.chunk.js.map